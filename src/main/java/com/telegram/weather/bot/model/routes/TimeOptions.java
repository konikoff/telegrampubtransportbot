package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.QueryTimeType;
import lombok.*;

import java.time.Instant;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TimeOptions {
    private Instant dateTime;
    private QueryTimeType queryTimeType;

}
