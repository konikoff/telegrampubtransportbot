package com.telegram.weather.bot.model.routes.enums;

public enum DiscountId {
    NORMAL, DISCOUNT
}
