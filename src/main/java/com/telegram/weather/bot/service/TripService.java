package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.GeoPoint;
import com.telegram.weather.bot.model.Trip;
import com.telegram.weather.bot.model.User;

public interface TripService {

    void saveStartPoint(GeoPoint startPoint, Chat chat);

    void saveDestinationPoint(GeoPoint startPoint, Chat chat);

    void save(Trip trip);

    Trip getLastTrip(Chat chat);
}
