package com.telegram.weather.bot.model.jakdojadequery;

import com.telegram.weather.bot.model.addresses.Address;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Location {
    private String name;
    private String subName;
    private String locationType;
    private Coordinate coordinate;
    private Address address;
}
