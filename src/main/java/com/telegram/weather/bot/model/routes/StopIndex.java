package com.telegram.weather.bot.model.routes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class StopIndex {
    private Integer partIndex;
    private String stopWithinPartIndex;
}
