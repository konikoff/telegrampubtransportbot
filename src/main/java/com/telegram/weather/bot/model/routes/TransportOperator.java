package com.telegram.weather.bot.model.routes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TransportOperator {
    private Integer transportOperatorId;
    private String transportOperatorSymbol;
    private String nameShortcut;
    private String transportOperatorName;
}
