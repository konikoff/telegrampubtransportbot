package com.telegram.weather.bot.model.addresses;

import com.telegram.weather.bot.model.jakdojadequery.Location;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class Addresses {
    private List<Location> locations;
}
