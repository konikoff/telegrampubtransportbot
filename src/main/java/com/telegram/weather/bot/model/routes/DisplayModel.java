package com.telegram.weather.bot.model.routes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DisplayModel {
    private String type;
    private String layout;
    private String headerBar;
    private String constraintText;
    private String title;
    private String subTitle;
    private String description;
}
