package com.telegram.weather.bot.controllers;

import com.telegram.weather.bot.annotation.BotCommand;
import com.telegram.weather.bot.annotation.BotController;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.service.ChatService;
import com.telegram.weather.bot.service.JakdojadeService;
import com.telegram.weather.bot.service.LocalisationService;
import com.telegram.weather.bot.utils.SendMessageUtil;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@BotController(value = StateCommand.DEFAULT)
public class DefaultController extends SendMessageExecutor {

    private final ChatService chatService;
    private final LocalisationService localisationService;
    private final JakdojadeService jakdojadeService;

    public DefaultController(ChatService chatService,
                             LocalisationService localisationService,
                             JakdojadeService jakdojadeService) {
        this.chatService = chatService;
        this.localisationService = localisationService;
        this.jakdojadeService = jakdojadeService;
    }

    @BotCommand(command = Command.DEFAULT)
    public void sendMessageDefault(Message message, String language) throws TelegramApiException {
        this.jakdojadeService.registerAnonymous(chatService.getChat(message));

        InlineKeyboardMarkup replyKeyboardMarkup = SendMessageUtil.getMainMenuKeyboard(localisationService, language);

        chatService.updateStatus(message, StateCommand.MAIN_MENU);

        getSender().execute(sendHelpMessage(message.getChatId(), message.getMessageId(), replyKeyboardMarkup, language));
    }

    private <T extends ReplyKeyboard> SendMessage sendHelpMessage(Long chatId, Integer messageId, T replyKeyboard, String language) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setReplyToMessageId(messageId);
        if (replyKeyboard != null) {
            sendMessage.setReplyMarkup(replyKeyboard);
        }
        String helpMessage = localisationService.getString("helpTransportMessage", language);
        sendMessage.setText(SendMessageUtil.prepareHelpMessage(helpMessage));
        return sendMessage;
    }
}
