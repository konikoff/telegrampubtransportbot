package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.User;
import com.telegram.weather.bot.model.repositories.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean save(User user) {
        return this.userRepository.save(user) != null;
    }
}
