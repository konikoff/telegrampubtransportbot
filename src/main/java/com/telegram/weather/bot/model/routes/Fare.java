package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.FareType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class Fare {
    private List<RoutePartsRanges> relatedRoutePartsRanges;
    private FareType fareType;
    private List<Ticket> tickets;
}
