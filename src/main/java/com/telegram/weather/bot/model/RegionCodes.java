package com.telegram.weather.bot.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "RegionCodes",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"cityName", "regionSymbol"})})
public class RegionCodes extends BaseEntity {

    @Column(name = "cityName")
    private String cityName;

    @Column(name = "regionSymbol")
    private String regionSymbol;
}
