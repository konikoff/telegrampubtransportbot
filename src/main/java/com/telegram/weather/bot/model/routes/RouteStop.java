package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.jakdojadequery.Coordinate;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class RouteStop {
    private Coordinate location;
    private List<Coordinate> shape;
    private LineStop lineStop;
    private DateTimeMarker departureTime;
    private DateTimeMarker arrivalTime;

}
