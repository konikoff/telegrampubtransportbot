package com.telegram.weather.bot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "Chat")
public class Chat extends BaseEntity {

    @Column(name = "chatId")
    private Long chatId;

    @Column(name = "state")
    private String state;

    @OneToOne(cascade = CascadeType.ALL)
    private User user;

    @OneToOne(mappedBy = "chat")
    private JakdojadeUser jakdojadeUser;
}
