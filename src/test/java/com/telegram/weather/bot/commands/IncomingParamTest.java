package com.telegram.weather.bot.commands;

import org.junit.Test;
import org.mockito.Mockito;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class IncomingParamTest {

    private final String COMMAND = "UPDATE_LANGUAGE:LANGUAGE=ENGLISH:CITY=New York";

    @Test
    public void checkParsingOfParam() {
        final Update update = Mockito.spy(new Update());

        when(update.getCallbackQuery()).thenReturn(new CallbackQuery());

        final CallbackQuery callbackQuery = Mockito.spy(update.getCallbackQuery());

        when(callbackQuery.getData()).thenReturn(COMMAND);

        BotParameters incomingParam = new BotParameters(update);

        assertEquals(Command.UPDATE_LANGUAGE, incomingParam.getCommand());
        assertEquals("ENGLISH", incomingParam.getParam("LANGUAGE"));
        assertEquals("New York", incomingParam.getParam("CITY"));

    }

    @Test
    public void createQuery() {
        BotParameters botParameters = new BotParameters();
        botParameters.setParam("LANGUAGE", "ENGLISH");
        botParameters.setParam("CITY", "New York");
        botParameters.setCommand(Command.UPDATE_LANGUAGE);

        assertEquals(COMMAND, botParameters.getParamQuery());
    }

}
