package com.telegram.weather.bot.model.addresses;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Address {
    private String addressCity;
    private String addressStreet;
    private String addressNumber;
    private String addressDistrict;
}
