package com.telegram.weather.bot.controllers;


import com.telegram.weather.bot.annotation.BotCommand;
import com.telegram.weather.bot.annotation.BotController;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.GeoPoint;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.service.ChatService;
import com.telegram.weather.bot.service.JakdojadeService;
import com.telegram.weather.bot.service.LocalisationService;
import com.telegram.weather.bot.service.TripService;
import com.telegram.weather.bot.utils.SendMessageUtil;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.ArrayList;
import java.util.List;

@BotController(value = StateCommand.TRIP_LOCATION_END)
public class TripEndpointController extends SendMessageExecutor {

    private final ChatService chatService;
    private final LocalisationService localisationService;
    private final TripService tripService;


    public TripEndpointController(ChatService chatService,
                                  LocalisationService localisationService,
                                  TripService tripService) {
        this.chatService = chatService;
        this.localisationService = localisationService;
        this.tripService = tripService;
    }

    @BotCommand(command = Command.DEFAULT)
    public void handleEndpoint(Message message, String language) throws TelegramApiException {

        if (message.hasLocation()) {
            chatService.updateStatus(message, StateCommand.TRIP_TIME_TYPE);
            Chat savedChat = chatService.getChat(message);

            final Float longitude = message.getLocation().getLongitude();
            final Float latitude = message.getLocation().getLatitude();

            this.tripService.saveDestinationPoint(new GeoPoint(longitude, latitude), savedChat);

            getSender().execute(getTripTimeTypeMessage(message, language));
        } else {
            getSender().execute(SendMessageUtil.sendHideKeyboard(message, localisationService.getString("endTripMessageError", language)));
        }
    }

    private SendMessage getTripTimeTypeMessage(Message message, String language) {
        SendMessage sendMessage = SendMessageUtil.replyOnPreviousMsgMarkdown(message);
        sendMessage.setReplyMarkup(getTripTimeTypeKeyboard(language))
                .setText(SendMessageUtil.getTripTimeTypeMessage(localisationService.getString("tripTimeType", language)));
        return sendMessage;
    }

    private ReplyKeyboardMarkup getTripTimeTypeKeyboard(String language) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true)
                .setResizeKeyboard(true)
                .setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();

        String arrivalMessage = localisationService.getString("arrival", language);
        keyboardFirstRow.add(SendMessageUtil.getArrivalCommand(arrivalMessage));

        String departureCommand = localisationService.getString("departure", language);
        keyboardFirstRow.add(SendMessageUtil.getDepartureCommand(departureCommand));

        keyboard.add(keyboardFirstRow);
        keyboard.add(new KeyboardRow() {{
            String nowCommand = localisationService.getString("now", language);
            add(new KeyboardButton(SendMessageUtil.getNowCommand(nowCommand)));
        }});

        keyboard.add(new KeyboardRow() {{
            String cancelCommand = localisationService.getString("cancel", language);
            add(new KeyboardButton(SendMessageUtil.getCancelCommand(cancelCommand)));
        }});

        replyKeyboardMarkup.setKeyboard(keyboard);

        return replyKeyboardMarkup;
    }


}
