package com.telegram.weather.bot.controller;

import com.telegram.weather.bot.annotation.processor.BotCommandProcessor;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.Commands;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.service.ChatService;
import com.telegram.weather.bot.service.Emoji;
import com.telegram.weather.bot.utils.SendMessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
@Profile("long-polling")
@PropertySource("classpath:telegram.properties")
public class TransportBotController extends TelegramLongPollingBot {
    private final Logger logger = LoggerFactory.getLogger(TransportBotController.class);

    @Value("${telegram.bot.token}")
    private String token;

    @Value("${telegram.bot.username}")
    private String botUsername;

    private final ChatService chatService;

    private final BotCommandProcessor botCommandProcessor;

    @Autowired
    public TransportBotController(ChatService chatService,
                                  BotCommandProcessor botCommandProcessor) {
        this.chatService = chatService;
        this.botCommandProcessor = botCommandProcessor;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasMessage()) {
                Message message = update.getMessage();
                if (message.hasText() || message.hasLocation()) {
                    handleMessage(message);
                }
            }
        } catch (Exception e) {
            logger.error("Exception", e);
        }
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return token;
    }


    private void handleMessage(Message message) throws TelegramApiException {
        SendMessage hideKeyboard = SendMessageUtil.sendHideKeyboard(message, Emoji.WAVING_HAND_SIGN.toString());

        final Chat savedChat = chatService.getChat(message);

        if (!message.isUserMessage() && message.hasText()) {
            if (isCommandForOther(message.getText())) {
                return;
            } else if (message.getText().startsWith(Commands.STOP)) {
                execute(hideKeyboard);
                chatService.updateStatus(message, StateCommand.START_STATE);
                return;
            }
        }

        if (message.hasText() || message.hasLocation()) {
            final String msgState = savedChat.getState();
            final String msgText = message.getText();
            final String language = savedChat.getUser().getLanguage();
            Object invoked = this.botCommandProcessor.invoke(msgState, msgText, message, language);
            if (invoked == null) {
                return; //FIXME: Temporal workaround for Void return type.
            } else if (invoked instanceof SendMessage) {
                //execute((SendMessage) invoked);

                return;
            } else if (invoked instanceof SendPhoto) {
                // execute((SendPhoto) invoked);

                return;
            }
        }

        execute(hideKeyboard);
    }


    private boolean isCommandForOther(String text) {
        boolean isSimpleCommand = "/start".equals(text) || "/help".equals(text) || "/stop".equals(text);
        boolean isCommandForMe = "/start@weatherbot".equals(text) || "/help@weatherbot".equals(text) || "/stop@weatherbot".equals(text);
        return "/".startsWith(text) && !isSimpleCommand && !isCommandForMe;
    }

}
