package com.telegram.weather.bot.controllers;

import com.telegram.weather.bot.annotation.BotCommand;
import com.telegram.weather.bot.annotation.BotController;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.service.ChatService;
import com.telegram.weather.bot.service.Emoji;
import com.telegram.weather.bot.service.LocalisationService;
import com.telegram.weather.bot.utils.SendMessageUtil;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@BotController(value = StateCommand.MAIN_MENU)
public class MainMenuController extends SendMessageExecutor {

    private final ChatService chatService;
    private final LocalisationService localisationService;

    public MainMenuController(ChatService chatService,
                              LocalisationService localisationService) {
        this.chatService = chatService;
        this.localisationService = localisationService;
    }

    @BotCommand(name = "trip", emoji = {Emoji.HEAVY_PLUS_SIGN}, command = Command.NEW_TRIP)
    public void newTrip(Message message, String language) throws TelegramApiException {

        final Chat saveChat = chatService.getChat(message);
        SendMessage sendMessage = SendMessageUtil.replyOnPreviousMsgMarkdown(message);

        if (saveChat.getUser().getRegionCodes() == null) {
            sendMessage = onSettingsChosen(message, Optional.of(SendMessageUtil.getWarningMessage(localisationService.getString("warning.city", language))), language);
        } else {
            sendMessage.setReplyMarkup(getRecentTripsKeyboard(message.getFrom().getId(), language, true));

            sendMessage.setText(localisationService.getString("newTrip", language));

            chatService.updateStatus(message, StateCommand.TRIP);
        }

        getSender().execute(sendMessage);
    }

    @BotCommand(name = "settings", emoji = Emoji.WRENCH, command = Command.SETTINGS)
    public void settings(Message message, String language) throws TelegramApiException {
        getSender().execute(onSettingsChosen(message, Optional.empty(), language));
    }

    private SendMessage onSettingsChosen(Message message, Optional<String> extraMessage, String language) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);

        sendMessage.setReplyMarkup(getSettingsKeyboard(language));
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setChatId(message.getChatId());

        String outgoingText = "";

        if (extraMessage.isPresent()) {
            outgoingText = extraMessage.get() + "\n";
        }

        outgoingText = outgoingText + getSettingsMessage(message, language);
        sendMessage.setText(outgoingText);

        chatService.updateStatus(message, StateCommand.SETTINGS);
        return sendMessage;
    }

    private String getSettingsMessage(Message message, String language) {
        Chat savedChat = chatService.getChat(message);

        String languageCode;
        if (!savedChat.getUser().getLanguage().isEmpty()) {
            languageCode = getLanguageExistDescription(savedChat.getUser().getLanguage(), language);
        } else {
            languageCode = getLanguageDescription(language);
        }

        String chooseCityMenu;
        if (savedChat.getUser().getRegionCodes() != null) {
            chooseCityMenu = getCityExistsDescription(savedChat.getUser().getRegionCodes().getCityName(), language);
        } else {
            chooseCityMenu = getCityDescription(language);
        }

        String baseString = localisationService.getString("onSettingsCommand", language);
        return String.format(baseString, Emoji.GLOBE_WITH_MERIDIANS.toString(), languageCode, Emoji.CITY_SUNRISE.toString(), chooseCityMenu, Emoji.BACK_WITH_LEFTWARDS_ARROW_ABOVE.toString());
    }

    private String getCityDescription(String language) {
        return localisationService.getString("cityDescription", language);
    }

    private String getCityExistsDescription(String city, String language) {
        return String.format(localisationService.getString("cityExistDescription", language), city);
    }

    private String getLanguageExistDescription(String languageCode, String language) {
        return String.format(localisationService.getString("languageExistDescription", language), languageCode);
    }

    private String getLanguageDescription(String language) {
        return localisationService.getString("languageDescription", language);
    }

    private InlineKeyboardMarkup getRecentTripsKeyboard(Integer userId, String language, boolean allowNew) {
        InlineKeyboardMarkup replyKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineKeyboardButtons = new ArrayList<>();


        if (allowNew) {
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            row1.add(new InlineKeyboardButton()
                    .setText(SendMessageUtil.getLocationCommand(localisationService.getString("locationStart", language)))
                    .setCallbackData(Command.DEFAULT.name()));
            inlineKeyboardButtons.add(row1);
            
        }
        List<InlineKeyboardButton> back = new ArrayList<>();
        back.add(new InlineKeyboardButton()
                .setText(SendMessageUtil.getBackCommand(localisationService.getString("back", language)))
                .setCallbackData(Command.BACK.name()));
        inlineKeyboardButtons.add(back);


        return replyKeyboardMarkup.setKeyboard(inlineKeyboardButtons);
    }

    private InlineKeyboardMarkup getSettingsKeyboard(String language) {
        InlineKeyboardMarkup replyKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineKeyboardButtons = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        row1.add(new InlineKeyboardButton()
                .setText(SendMessageUtil.getLanguagesCommand(localisationService.getString("languages", language)))
                .setCallbackData(Command.LANGUAGE_SETTINGS.name()));
        row1.add(new InlineKeyboardButton()
                .setText(SendMessageUtil.getChooseCity(localisationService.getString("city", language)))
                .setCallbackData(Command.CITY_SETTINGS.name()));
        inlineKeyboardButtons.add(row1);

        List<InlineKeyboardButton> back = new ArrayList<>();
        back.add(new InlineKeyboardButton()
                .setText(SendMessageUtil.getBackCommand(localisationService.getString("back", language)))
                .setCallbackData(Command.BACK.name()));
        inlineKeyboardButtons.add(back);


        return replyKeyboardMarkup.setKeyboard(inlineKeyboardButtons);
    }
}
