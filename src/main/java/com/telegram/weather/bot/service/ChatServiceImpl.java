package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.model.User;
import com.telegram.weather.bot.model.repositories.ChatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Message;

import javax.transaction.Transactional;

@Slf4j
@Service
public class ChatServiceImpl implements ChatService {

    private final ChatRepository chatRepository;

    public ChatServiceImpl(ChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }

    @Override
    @Transactional
    public Chat getChat(Message message) {
        final Long chatId = message.getChatId();
        final Long userId = Long.valueOf(message.getFrom().getId());
        final String languageCode = message.getFrom().getLanguageCode();

        Chat savedChat = chatRepository.getChatByChatId(chatId);
        if (savedChat == null) {
            return initChat(chatId, userId, languageCode);
        }

        return savedChat;
    }

    /**
     * Invoke if the chat wasn't found in DB
     *
     * @param chatId
     * @param userId
     * @return - saved chat
     */
    private Chat initChat(Long chatId, Long userId, String languageCode) {
        log.debug("Initialisation of the chat: " + chatId + ", with new userId: " + userId);
        Chat chat = new Chat();
        chat.setState(StateCommand.DEFAULT);
        chat.setChatId(chatId);

        User user = new User();
        user.setChat(chat);
        user.setUserId(userId);
        user.setLanguage(languageCode);

        chat.setUser(user);
        return chatRepository.save(chat);
    }

    @Override
    public Chat updateStatus(Message message, String state) {
        Chat savedChat = getChat(message);
        savedChat.setState(state);

        return chatRepository.save(savedChat);
    }
}
