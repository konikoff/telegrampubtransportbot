package com.telegram.weather.bot.controllers;

import com.telegram.weather.bot.annotation.BotCommand;
import com.telegram.weather.bot.annotation.BotController;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.model.User;
import com.telegram.weather.bot.model.repositories.UserRepository;
import com.telegram.weather.bot.service.ChatService;
import com.telegram.weather.bot.service.Emoji;
import com.telegram.weather.bot.service.LocalisationService;
import com.telegram.weather.bot.service.LocalisationServiceImpl;
import com.telegram.weather.bot.utils.SendMessageUtil;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@BotController(value = StateCommand.SETTINGS_LANGUAGE)
public class SettingsLanguage extends SendMessageExecutor {

    private final ChatService chatService;
    private final LocalisationService localisationService;
    private final UserRepository userRepository;

    private final MainMenuController mainMenuController;

    public SettingsLanguage(ChatService chatService,
                            LocalisationService localisationService,
                            UserRepository userRepository,
                            MainMenuController mainMenuController) {
        this.chatService = chatService;
        this.localisationService = localisationService;
        this.userRepository = userRepository;
        this.mainMenuController = mainMenuController;
    }

    @BotCommand(command = Command.DEFAULT)
    public void handleLanguage(Message message, String language) throws TelegramApiException {
        if (message.hasText()) {
            if (localisationService.getLanguageByName(message.getText().trim()) != null) { //Set language
                getSender().execute(onLanguageChosen(message));
            } else { //Unsupported language
                getSender().execute(onLanguageError(message, language));
            }
        }

        throw new RuntimeException("Expected text message, but received unsupported: " + message);
    }

    @BotCommand(name = "back", emoji = Emoji.BACK_WITH_LEFTWARDS_ARROW_ABOVE, command = Command.BACK)
    public void back(Message message, String language) throws TelegramApiException {
        mainMenuController.settings(message, language);
    }

    @BotCommand(command = Command.UPDATE_LANGUAGE)
    public void updateLanguage(Message message, String language) throws TelegramApiException {
        //TODO: Implement it
    }

    private SendMessage onLanguageChosen(Message message) {
        String languageCode = localisationService.getLanguageCodeByName(message.getText().trim());

        Chat savedChat = chatService.getChat(message);
        User savedUser = savedChat.getUser();
        savedUser.setLanguage(message.getText().trim());
        userRepository.save(savedUser);

        SendMessage sendMessageRequest = new SendMessage();
        sendMessageRequest.enableMarkdown(true);
        sendMessageRequest.setChatId(message.getChatId().toString());
        sendMessageRequest.setText(localisationService.getString("languageUpdated", languageCode));
        sendMessageRequest.setReplyToMessageId(message.getMessageId());
        sendMessageRequest.setReplyMarkup(SendMessageUtil.getMainMenuKeyboard(localisationService, languageCode));

        chatService.updateStatus(message, StateCommand.MAIN_MENU);
        return sendMessageRequest;
    }

    private InlineKeyboardMarkup getLanguagesKeyboard(String language) {
        InlineKeyboardMarkup replyKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineKeyboardButtons = new ArrayList<>();

        for (String languageName : localisationService.getSupportedLanguages().stream().map(
                LocalisationServiceImpl.Language::getName).collect(Collectors.toList())) {

            List<InlineKeyboardButton> rowKeyboard = new ArrayList<>();
            rowKeyboard.add(new InlineKeyboardButton()
                    .setText(languageName)
                    .setCallbackData(Command.UPDATE_LANGUAGE.name()));
            inlineKeyboardButtons.add(rowKeyboard);
        }
        List<InlineKeyboardButton> backKeyboard = new ArrayList<>();
        backKeyboard.add(new InlineKeyboardButton()
                .setText(SendMessageUtil.getBackCommand(language))
                .setCallbackData(Command.BACK.name()));
        inlineKeyboardButtons.add(backKeyboard);

        return replyKeyboardMarkup.setKeyboard(inlineKeyboardButtons);
    }

    private SendMessage onLanguageError(Message message, String language) {
        SendMessage sendMessageRequest = new SendMessage();
        sendMessageRequest.enableMarkdown(true);
        sendMessageRequest.setChatId(message.getChatId().toString());
        sendMessageRequest.setReplyMarkup(getLanguagesKeyboard(language));
        sendMessageRequest.setText(localisationService.getString("errorLanguageNotFound", language));
        sendMessageRequest.setReplyToMessageId(message.getMessageId());

        return sendMessageRequest;
    }
}
