package com.telegram.weather.bot.service.parser;

import com.telegram.weather.bot.model.RegionCodes;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.util.Set;

import static org.junit.Assert.*;

public class ParseRegionsTest {

    @Rule
    public ResourceFile res = new ResourceFile("/regions.json");

    @Test
    public void unmarshalJsonRoutes() throws IOException {
        assertTrue(res.getFile().exists());
        Set<RegionCodes> regionCodes = ParseRegions.unmarshalJsonRoutes(res.getContent());
        assertEquals(47, regionCodes.size());
    }
}