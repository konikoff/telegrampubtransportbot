package com.telegram.weather.bot.controllers;

import com.telegram.weather.bot.annotation.BotCommand;
import com.telegram.weather.bot.annotation.BotController;
import com.telegram.weather.bot.annotation.BotRequestParam;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.RegionCodes;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.model.User;
import com.telegram.weather.bot.service.*;
import com.telegram.weather.bot.utils.SendMessageUtil;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@BotController(value = StateCommand.SETTINGS_CITY)
public class SettingCitiesController extends SendMessageExecutor {

    private final RegionCodeService regionCodeService;
    private final LocalisationService localisationService;
    private final ChatService chatService;
    private final UserService userService;


    private final MainMenuController mainMenuController;

    public SettingCitiesController(RegionCodeService regionCodeService,
                                   LocalisationService localisationService,
                                   ChatService chatService,
                                   UserService userService, MainMenuController mainMenuController) {
        this.regionCodeService = regionCodeService;
        this.localisationService = localisationService;
        this.chatService = chatService;
        this.userService = userService;
        this.mainMenuController = mainMenuController;
    }

    // FIXME: Implement handling of Cities
    @BotCommand(command = Command.DEFAULT)
    public void handleCity(Message message, String language) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setChatId(message.getChatId());

        RegionCodes regionCode = this.regionCodeService.findByName(message.getText());
        if (regionCode != null) {

            final Chat saveChat = chatService.getChat(message);
            User user = saveChat.getUser();
            user.setRegionCodes(regionCode);
            this.userService.save(user);

            sendMessage.setText(getChosenCityMessage(regionCode.getCityName(), language));
            sendMessage.setReplyMarkup(SendMessageUtil.getMainMenuKeyboard(localisationService, language));
            chatService.updateStatus(message, StateCommand.MAIN_MENU);

        } else {
            sendMessage.setText(getIncorrectChosenCityMessage(message.getText(), language));
            chatService.updateStatus(message, StateCommand.SETTINGS_CITY);
        }
        getSender().execute(sendMessage);
    }

    @BotCommand(command = Command.UPDATE_CITY)
    public void updateCity(Message message, String language, @BotRequestParam(name = "city_name") String cityName) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setChatId(message.getChatId());

        RegionCodes regionCode = this.regionCodeService.findByName(cityName);
        if (regionCode != null) {

            final Chat saveChat = chatService.getChat(message);
            User user = saveChat.getUser();
            user.setRegionCodes(regionCode);
            this.userService.save(user);

            sendMessage.setText(getChosenCityMessage(regionCode.getCityName(), language));
            sendMessage.setReplyMarkup(SendMessageUtil.getMainMenuKeyboard(localisationService, language));
            chatService.updateStatus(message, StateCommand.MAIN_MENU);

        } else {
            sendMessage.setText(getIncorrectChosenCityMessage(message.getText(), language));
            chatService.updateStatus(message, StateCommand.SETTINGS_CITY);
        }
        getSender().execute(sendMessage);
    }


    @BotCommand(name = "back", emoji = Emoji.BACK_WITH_LEFTWARDS_ARROW_ABOVE, command = Command.BACK)
    public void back(Message message, String language) throws TelegramApiException {
        mainMenuController.settings(message, language);
    }

    private String getChosenCityMessage(String city, String language) {
        return String.format(this.localisationService.getString("chosenCityMessage", language), city);
    }

    private String getIncorrectChosenCityMessage(String city, String language) {
        return String.format(this.localisationService.getString("incorrectChosenCityMessage", language), city);
    }
}
