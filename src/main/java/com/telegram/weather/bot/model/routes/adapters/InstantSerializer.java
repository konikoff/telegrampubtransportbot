package com.telegram.weather.bot.model.routes.adapters;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.*;
import java.time.format.DateTimeFormatter;

public class InstantSerializer implements JsonSerializer<Instant> {
    @Override
    public JsonElement serialize(Instant src, Type typeOfSrc, JsonSerializationContext context) {
        ZoneId warsawTimeZone = ZoneId.of("Europe/Warsaw");

        ZonedDateTime zonedDateTime = src.atZone(warsawTimeZone);

        String formattedDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX").format(zonedDateTime);
        return new JsonPrimitive(formattedDateTime);
    }
}
