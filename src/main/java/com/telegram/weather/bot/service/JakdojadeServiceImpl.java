package com.telegram.weather.bot.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.telegram.weather.bot.commands.RouteCommand;
import com.telegram.weather.bot.converters.AddressesToAddressConverter;
import com.telegram.weather.bot.model.*;
import com.telegram.weather.bot.model.addresses.Addresses;
import com.telegram.weather.bot.model.repositories.RegionRepository;
import com.telegram.weather.bot.model.routes.*;
import com.telegram.weather.bot.model.routes.enums.*;
import com.telegram.weather.bot.model.jakdojadequery.*;
import com.telegram.weather.bot.model.jakdojadequery.enums.*;
import com.telegram.weather.bot.service.parser.ParseRegions;
import com.telegram.weather.bot.utils.JakdojadeUtils;
import com.telegram.weather.bot.utils.ParseUtils;
import com.telegram.weather.bot.utils.Signature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Set;

@Slf4j
@Service
public class JakdojadeServiceImpl implements JakdojadeService {


    private final RestTemplate restTemplate;
    private final JakdojadeUserService jakdojadeUserService;
    private final RegionRepository regionRepository;
    private final AddressesToAddressConverter addressConverter;

    public JakdojadeServiceImpl(RestTemplate restTemplate,
                                JakdojadeUserService jakdojadeUserService,
                                RegionRepository regionRepository,
                                AddressesToAddressConverter addressConverter) {
        this.restTemplate = restTemplate;
        this.jakdojadeUserService = jakdojadeUserService;
        this.regionRepository = regionRepository;
        this.addressConverter = addressConverter;
    }


    @Override
    public void registerAnonymous(Chat chat) {
        if (!jakdojadeUserService.isRegistered(chat)) {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(JakdojadeUtils.URI).
                    path(JakdojadeUtils.REGISTER_ANONYMOUS);

            final String userDeviceId = JakdojadeUtils.computeUserDeviceId();

            HttpHeaders headers = new HttpHeaders();
            headers.set(JakdojadeUtils.USER_DEVICE_ID, userDeviceId);
            HttpEntity httpEntity = new HttpEntity(headers);
            ResponseEntity<String> stringResponseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, httpEntity, String.class);

            if (stringResponseEntity.getStatusCode() == HttpStatus.OK) {
                Gson gson = new GsonBuilder().create();
                JsonObject registerData = gson.fromJson(stringResponseEntity.getBody(), JsonObject.class);
                JsonElement profileLoginObject = registerData.get("userProfile").getAsJsonObject().get("profileLogin");
                JsonElement passwordHashObject = registerData.get("userProfile").getAsJsonObject().get("passwordHash");

                JakdojadeUser jakdojadeUser = new JakdojadeUser();
                jakdojadeUser.setUserDeviceId(userDeviceId);
                jakdojadeUser.setProfileLogin(profileLoginObject.getAsString());
                jakdojadeUser.setPasswordHash(passwordHashObject.getAsString());
                jakdojadeUser.setChat(chat);

                jakdojadeUserService.register(jakdojadeUser);
            } else {
                throw new RuntimeException("Status: " + stringResponseEntity.getStatusCode() + ", Body:" + stringResponseEntity.getBody());
            }
        } else {
            log.info("The chat: " + chat.getChatId() + " is already registered!");
        }

    }

    @Override
    public void fetchAllRegions(Chat chat) {
        if (!jakdojadeUserService.isRegistered(chat)) {
            registerAnonymous(chat);
        }
        JakdojadeUser jakdojadeUser = this.jakdojadeUserService.getJakdojadeUserByChat(chat);
        getRegions(jakdojadeUser);
    }

    @Override
    public Routes getRoutes(RouteCommand routeCommand) {
        JakdojadeUser jakdojadeUser = this.jakdojadeUserService.getJakdojadeUserByChat(routeCommand.getChat());

        JakdojadeQuery jakdojadeQuery = prepareSearchQuery(routeCommand);

        String routesJson = requestOfRoutes(jakdojadeQuery, jakdojadeUser);
        return ParseUtils.parseRoutes(routesJson);
    }

    private JakdojadeQuery prepareSearchQuery(RouteCommand routeCommand) {

        Trip lastTrip = routeCommand.getLastTrip();

        Coordinate startCoordinate = new Coordinate();
        startCoordinate.setXLon(String.valueOf(lastTrip.getStartPoint().getGeoPoint().getLongitude()));
        startCoordinate.setYLat(String.valueOf(lastTrip.getStartPoint().getGeoPoint().getLatitude()));

        Coordinate destinationCoordinate = new Coordinate();
        destinationCoordinate.setXLon(String.valueOf(lastTrip.getEndPoint().getGeoPoint().getLongitude()));
        destinationCoordinate.setYLat(String.valueOf(lastTrip.getEndPoint().getGeoPoint().getLatitude()));

        SearchQuery searchQuery = SearchQuery.builder()
                .routesCount(3) // default value to fetch 3 routes
                .start(new Point(routeCommand.getChat().getUser().getRegionCodes().getRegionSymbol(), startCoordinate))
                .destination(new Point(routeCommand.getChat().getUser().getRegionCodes().getRegionSymbol(), destinationCoordinate))
                .timeOptions(new TimeOptions(routeCommand.getDateTime(), lastTrip.getQueryTimeType()))
                .allowedRouteTypes(new RouteType[]{RouteType.PUBLIC_TRANSPORT})
                .realtimeSearchMode(SearchMode.NO_REALTIME)
                .publicTransportOptions(new TransportOptions(AvoidChanges.DEFAULT, AccessibilityOptions.NONE))
                .userConnectionTypePreference(ConnectionTypePreference.OPTIMAL)
                .build();

        return JakdojadeQuery.builder()
                .fetchType(FetchType.SYNC)
                .userLocation(null)
                .routesCorrelation(RoutesCorrelation.NONE)
                .searchQuery(searchQuery)
                .build();
    }

    private String requestOfRoutes(JakdojadeQuery prepareQuery, JakdojadeUser jakdojadeUser) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(JakdojadeUtils.URI).path(JakdojadeUtils.ROUTES);

        String searchQueryJson = ParseUtils.jakdojadeQueryToJson(prepareQuery);

        HttpHeaders httpHeaders = prepareHeaders(builder.toUriString(), jakdojadeUser);
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> httpEntity = new HttpEntity<>(searchQueryJson, httpHeaders);


        ResponseEntity<String> stringResponseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, httpEntity, String.class);
        log.debug(stringResponseEntity.getHeaders().toString());

        return stringResponseEntity.getBody();
    }


    private void getRegions(JakdojadeUser jakdojadeUser) {
        if (this.regionRepository.count() <= 0) {
            UriComponentsBuilder builder = UriComponentsBuilder
                    .fromUriString(JakdojadeUtils.URI)
                    .path(JakdojadeUtils.REGIONS)
                    .queryParam("countryCode", "PL");

            HttpHeaders httpHeaders = prepareHeaders(builder.toUriString(), jakdojadeUser);
            httpHeaders.set("Referer", "https://jakdojade.pl/lista-miast");

            HttpEntity httpEntity = new HttpEntity(httpHeaders);
            ResponseEntity<String> stringResponseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, httpEntity, String.class);

            Set<RegionCodes> regionCodes = ParseRegions.unmarshalJsonRoutes(stringResponseEntity.getBody());
            this.regionRepository.saveAll(regionCodes);
        }
    }

    private HttpHeaders prepareHeaders(String URL, JakdojadeUser jakdojadeUser) {
        final String timestamp = String.valueOf(JakdojadeUtils.getTimestamp());
        HttpHeaders headers = new HttpHeaders();

        headers.set(JakdojadeUtils.PARAM_PROFILE_LOGIN, jakdojadeUser.getProfileLogin());
        headers.set(JakdojadeUtils.USER_DEVICE_ID, jakdojadeUser.getUserDeviceId());
        headers.set(JakdojadeUtils.SECURITY_VERSION, JakdojadeUtils.SECURITY_VERSION_VALUE);

        Signature signature = new Signature(URL, jakdojadeUser, timestamp);
        headers.set(JakdojadeUtils.SIGN, JakdojadeUtils.computeSign(signature));
        headers.set(JakdojadeUtils.TIMESTAMP, timestamp);

        return headers;
    }

    /*
        https://jakdojade.pl/api/web/v1/addresses?location=54.4063304:18.5957872
     */
    @Override
    public Address getAddressByCoordinate(Chat chat, GeoPoint geoPoint) {
        JakdojadeUser jakdojadeUser = this.jakdojadeUserService.getJakdojadeUserByChat(chat);


        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(JakdojadeUtils.URI)
                .path(JakdojadeUtils.ADDRESSES)
                .queryParam("location", geoPoint.getLatitude() + ":" + geoPoint.getLongitude());

        HttpHeaders httpHeaders = prepareHeaders(builder.toUriString(), jakdojadeUser);
        httpHeaders.set("Referer", "https://jakdojade.pl/lista-miast");
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<String> stringResponseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, httpEntity, String.class);

        Addresses addresses = ParseUtils.parseAddresses(stringResponseEntity.getBody());
        try {
            return addressConverter.convert(addresses);
        } catch (RuntimeException e) {//TODO: create exception class
            Address address = new Address();
            address.setGeoPoint(geoPoint);
            return address;
        }
    }
}
