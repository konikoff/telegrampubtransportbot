package com.telegram.weather.bot.service;


import com.telegram.weather.bot.model.Address;
import com.telegram.weather.bot.model.GeoPoint;

public interface NominatimGeoService {

    Address getAddressByCoordinate(GeoPoint geoPoint);

    GeoPoint getCoordinateByAddress(Address address);
}
