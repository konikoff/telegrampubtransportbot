package com.telegram.weather.bot.webhook.controllers;

import com.telegram.weather.bot.annotation.processor.BotCommandProcessor;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.controller.TransportBotWebhookController;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.service.ChatService;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/webhook/api")
public class WebhookRESTController {

    private final TransportBotWebhookController botWebhookController;
    private final BotCommandProcessor botCommandProcessor;
    private final ChatService chatService;

    public WebhookRESTController(TransportBotWebhookController botWebhookController,
                                 BotCommandProcessor botCommandProcessor,
                                 ChatService chatService) {
        this.botWebhookController = botWebhookController;
        this.botCommandProcessor = botCommandProcessor;
        this.chatService = chatService;
    }

    @GetMapping("/test")
    public ResponseObject testMethod() {

        return ResponseObject.builder().testString("Some response").build();
    }

    @PostMapping(value = "", consumes = "application/json")
    public void listenPOSTRequests(@RequestBody Update update, HttpServletRequest request) {

        //this.botWebhookController.onWebhookUpdateReceived(update);

        if (update.getMessage() != null && update.getMessage().hasText()) {
            log.info("Message: " + update.getMessage().getText());
        } else if (update.getCallbackQuery() != null) {
            log.info("Callback data: " + update.getCallbackQuery().getData());
        } else {
            log.info("Default update message: " + update.toString());
        }


        Message receivedMessage = null;
        //Command receivedCommand = Command.DEFAULT;
        if (update.getMessage() != null) {
            receivedMessage = update.getMessage();
        } else if (update.getCallbackQuery() != null) {
            receivedMessage = update.getCallbackQuery().getMessage();
            //receivedCommand = Command.valueOf(update.getCallbackQuery().getData());
        }


        final Chat savedChat = chatService.getChat(receivedMessage);
        final String msgState = savedChat.getState();
        final String language = savedChat.getUser().getLanguage();

        Object invoked = this.botCommandProcessor.executeCommand(msgState, update, receivedMessage, language);
        if (invoked == null) {
            return; //FIXME: Temporal workaround for Void return type.
        } else if (invoked instanceof SendMessage) {
            //execute((SendMessage) invoked);

            return;
        } else if (invoked instanceof SendPhoto) {
            // execute((SendPhoto) invoked);

            return;
        }
    }

    @Getter
    @Setter
    @Builder
    private static class ResponseObject {
        private String testString;
    }


}
