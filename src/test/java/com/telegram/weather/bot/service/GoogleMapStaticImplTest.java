package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.Trip;
import com.telegram.weather.bot.model.routes.Routes;
import com.telegram.weather.bot.service.parser.ResourceFile;
import com.telegram.weather.bot.utils.ParseUtils;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

public class GoogleMapStaticImplTest {

    @Rule
    public ResourceFile res = new ResourceFile("/route.json");

    @Test
    public void getRandomColor() {
        GoogleMapStaticImpl googleMapStatic = new GoogleMapStaticImpl();
        System.out.println(googleMapStatic.getRandomColor());
        assertTrue(googleMapStatic.getRandomColor().startsWith("0x"));
    }

    @Test
    public void getImageByRoute() throws IOException {
        GoogleMapStaticImpl googleMapStatic = new GoogleMapStaticImpl();
        Routes routes = ParseUtils.parseRoutes(res.getContent());
        InputStream image = googleMapStatic.getImageByRoute(new Trip(), routes.getRoutes().get(0));
        assertNotNull(image);
    }
}
