package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.RoutePartType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoutePart {
    private RoutePartType routePartType;
    private DateTimeMarker startDeparture;
    private DateTimeMarker targetArrival;
    private Integer routePartDistanceMeters;
    private RouteWalk routeWalk;
    private RouteVehicle routeVehicle;
}
