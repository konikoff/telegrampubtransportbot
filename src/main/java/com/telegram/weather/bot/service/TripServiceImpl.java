package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.*;
import com.telegram.weather.bot.model.repositories.TripRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TripServiceImpl implements TripService {

    private final TripRepository tripRepository;
    private final JakdojadeService jakdojadeService;


    public TripServiceImpl(TripRepository tripRepository,
                           JakdojadeService jakdojadeService) {
        this.tripRepository = tripRepository;
        this.jakdojadeService = jakdojadeService;
    }


    @Override
    public void saveStartPoint(GeoPoint startPoint, Chat chat) {
        Trip trip = new Trip();

        Address startPointAddress = jakdojadeService.getAddressByCoordinate(chat, startPoint);

        trip.setStartPoint(startPointAddress);
        trip.setTimestamp(Instant.now());

        trip.setUser(chat.getUser());
        tripRepository.save(trip);
    }

    @Override
    public void saveDestinationPoint(GeoPoint endPoint, Chat chat) {
        Optional<List<Trip>> tripList = tripRepository.findTripByUserWhereEndpointIsEmpty(chat.getUser());
        if (tripList.isPresent()) {
            Trip lastUnfinishedTrip = tripList.get().get(0);

            Address destinationPointAddress = jakdojadeService.getAddressByCoordinate(chat, endPoint);

            lastUnfinishedTrip.setEndPoint(destinationPointAddress);
            lastUnfinishedTrip.setTimestamp(Instant.now());

            tripRepository.save(lastUnfinishedTrip);

            log.debug("Successfully saved destination point for userId: " + chat.getUser().getUserId());
        } else {
            log.error("Unfortunately was not found any unfinished trips");
            throw new RuntimeException("Unfortunately was not found any unfinished trips");
        }
    }

    @Override
    public void save(Trip trip) {
        this.tripRepository.save(trip);
    }

    /**
     * @param chat
     * @return
     */
    @Override
    public Trip getLastTrip(Chat chat) {
        Optional<List<Trip>> tripList = tripRepository.findTripsByUser(chat.getUser());
        if (tripList.isPresent()) {
            return tripList.get().get(0);
        } else {
            log.error("Unfortunately was not found any unfinished trips");
            throw new RuntimeException("Unfortunately was not found any unfinished trips");
        }
    }
}
