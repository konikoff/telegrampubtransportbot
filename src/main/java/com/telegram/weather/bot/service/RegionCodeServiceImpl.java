package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.RegionCodes;
import com.telegram.weather.bot.model.repositories.RegionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class RegionCodeServiceImpl implements RegionCodeService {

    private final RegionRepository regionRepository;

    public RegionCodeServiceImpl(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Override
    public List<RegionCodes> getAll() {
        return this.regionRepository.findAll();
    }

    @Override
    public RegionCodes findByName(String cityName) {
        return this.regionRepository.findOneByName(cityName).orElseThrow(() -> new RuntimeException(String.format("Entered city: %s is not found!", cityName)));
    }
}
