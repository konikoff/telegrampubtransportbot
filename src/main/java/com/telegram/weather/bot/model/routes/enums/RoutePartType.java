package com.telegram.weather.bot.model.routes.enums;

public enum RoutePartType {
    WALK, VEHICLE_TRANSPORT, UNKNOWN
}
