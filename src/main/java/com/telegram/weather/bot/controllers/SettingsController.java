package com.telegram.weather.bot.controllers;

import com.telegram.weather.bot.annotation.BotCommand;
import com.telegram.weather.bot.annotation.BotController;
import com.telegram.weather.bot.commands.BotParameters;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.RegionCodes;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.service.*;
import com.telegram.weather.bot.utils.SendMessageUtil;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@BotController(value = StateCommand.SETTINGS)
public class SettingsController extends SendMessageExecutor {
    private final ChatService chatService;
    private final LocalisationService localisationService;
    private final RegionCodeService regionCodeService;
    private final DefaultController defaultController;
    private final JakdojadeService jakdojadeService;


    public SettingsController(ChatService chatService,
                              LocalisationService localisationService,
                              RegionCodeService regionCodeService, DefaultController defaultController,
                              JakdojadeService jakdojadeService) {
        this.chatService = chatService;
        this.localisationService = localisationService;
        this.regionCodeService = regionCodeService;
        this.defaultController = defaultController;
        this.jakdojadeService = jakdojadeService;
    }

    @BotCommand(name = "languages", emoji = Emoji.GLOBE_WITH_MERIDIANS, command = Command.DEFAULT)
    public void languages(Message message, String language) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);

        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setChatId(message.getChatId());
        sendMessage.setReplyMarkup(getLanguagesKeyboard(language));

        Chat savedChat = chatService.getChat(message);

        String languageCode = savedChat.getUser().getLanguage();
        String languagesMsg;
        if (languageCode != null && !languageCode.isEmpty()) {
            languagesMsg = String.format(localisationService.getString("selectLanguage", language), languageCode);
        } else {
            languagesMsg = localisationService.getString("requestForLanguage", language);
        }

        sendMessage.setText(languagesMsg);

        chatService.updateStatus(message, StateCommand.SETTINGS_LANGUAGE);
        getSender().execute(sendMessage);
    }

    @BotCommand(name = "city", emoji = Emoji.CITY_SUNRISE, command = Command.CITY_SETTINGS)
    public void city(Message message, String language) throws TelegramApiException {
        Chat savedChat = chatService.getChat(message);
        jakdojadeService.fetchAllRegions(savedChat);

        SendMessage sendMessage = new SendMessage();

        sendMessage.enableMarkdown(true);

        InlineKeyboardMarkup replyKeyboardMarkup = getCitiesKeyboard(language);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setChatId(message.getChatId());
        sendMessage.setText(localisationService.getString("citiesMessage", language));

        chatService.updateStatus(message, StateCommand.SETTINGS_CITY);
        getSender().execute(sendMessage);
    }

    @BotCommand(name = "back", emoji = Emoji.BACK_WITH_LEFTWARDS_ARROW_ABOVE, command = Command.BACK)
    public void back(Message message, String language) throws TelegramApiException {
        defaultController.sendMessageDefault(message, language);
    }

    private InlineKeyboardMarkup getCitiesKeyboard(String language) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineKeyboardButton = new ArrayList<>();

        List<InlineKeyboardButton> rows = new ArrayList<>();
        List<RegionCodes> regionCodes = this.regionCodeService.getAll();
        for (int i = 0, count = 0; i < regionCodes.size(); i++) {
            count++;

            if (count == 2) {
                inlineKeyboardButton.add(rows);

                rows = new ArrayList<>();
                count = 0;
            }

            BotParameters params = new BotParameters();

            rows.add(new InlineKeyboardButton()
                    .setText(regionCodes.get(i).getCityName())
                    .setCallbackData(params
                            .setCommand(Command.UPDATE_CITY)
                            .setParam("city_name", regionCodes.get(i).getCityName())
                            .getParamQuery()));

        }


        List<InlineKeyboardButton> backButton = new ArrayList<>();
        backButton.add(new InlineKeyboardButton()
                .setText(SendMessageUtil.getBackCommand(localisationService.getString("back", language)))
                .setCallbackData(Command.BACK.name()));
        inlineKeyboardButton.add(backButton);


        return inlineKeyboardMarkup.setKeyboard(inlineKeyboardButton);
    }

    private InlineKeyboardMarkup getLanguagesKeyboard(String language) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineKeyboardButton = new ArrayList<>();

        localisationService.getSupportedLanguages()
                .stream()
                .map(LocalisationServiceImpl.Language::getName)
                .forEach(languageName -> {
                    List<InlineKeyboardButton> row1 = new ArrayList<>();
                    row1.add(new InlineKeyboardButton()
                            .setText(languageName)
                            .setCallbackData(Command.UPDATE_LANGUAGE.name()));
                    inlineKeyboardButton.add(row1);
                });

        List<InlineKeyboardButton> backButton = new ArrayList<>();
        backButton.add(new InlineKeyboardButton()
                .setText(SendMessageUtil.getBackCommand(localisationService.getString("back", language)))
                .setCallbackData(Command.BACK.name()));
        inlineKeyboardButton.add(backButton);

        return inlineKeyboardMarkup.setKeyboard(inlineKeyboardButton);
    }
}
