package com.telegram.weather.bot.model.routes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class RouteFare {
    private List<Fare> fares;
}
