package com.telegram.weather.bot.service.test;

import com.telegram.weather.bot.model.Address;
import com.telegram.weather.bot.model.GeoPoint;
import com.telegram.weather.bot.service.NominatimGeoService;
import com.telegram.weather.bot.service.NominatimGeoServiceImpl;

public class TestNominatimService {

    public static void main(String[] args) {
        NominatimGeoService nominatimGeoService = new NominatimGeoServiceImpl();

        Float lat = 54.405743f;
        Float lon = 18.595081f;


        GeoPoint geoPoint = new GeoPoint();
        geoPoint.setLatitude(lat);
        geoPoint.setLongitude(lon);

        System.out.println(nominatimGeoService.getAddressByCoordinate(geoPoint).toString());

        Address adrKolobrzeska = new Address();

        adrKolobrzeska.setRoad("Kolobrzeska");
        adrKolobrzeska.setHouseNumber("55C");
        adrKolobrzeska.setCity("Gdansk");

        GeoPoint point = nominatimGeoService.getCoordinateByAddress(adrKolobrzeska);

        System.out.println("Lat: " + point.getLatitude() + ", Lon: " + point.getLongitude());

    }
}
