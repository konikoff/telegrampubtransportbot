package com.telegram.weather.bot.configs.interceptors;

import com.telegram.weather.bot.utils.JakdojadeUtils;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.Collections;

public class RequestInterceptor implements ClientHttpRequestInterceptor {
    private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        request.getHeaders().set("User-Agent", USER_AGENT);
        request.getHeaders().set(JakdojadeUtils.PARAM_LOCALE, "en");
        request.getHeaders().set(JakdojadeUtils.PARAM_APPV, "web");
        request.getHeaders().setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return execution.execute(request, body);
    }


}
