package com.telegram.weather.bot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "Address")
public class Address extends BaseEntity {

    @Column(name = "houseNumber")
    private String houseNumber;

    @Column(name = "road")
    private String road;

    @Column(name = "neighbourhood")
    private String neighbourhood;

    @Column(name = "suburb")
    private String suburb;

    @Column(name = "city")
    private String city;

    @Column(name = "county")
    private String county;

    @Column(name = "country")
    private String country;

    @Column(name = "state")
    private String state;

    @Column(name = "postcode")
    private String postcode;

    @Column(name = "geoPoint")
    private GeoPoint geoPoint;
}
