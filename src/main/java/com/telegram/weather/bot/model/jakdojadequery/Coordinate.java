package com.telegram.weather.bot.model.jakdojadequery;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Coordinate {
    @SerializedName("x_lon")
    private String xLon;

    @SerializedName("y_lat")
    private String yLat;
}
