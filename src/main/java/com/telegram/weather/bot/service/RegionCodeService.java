package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.RegionCodes;

import java.util.List;

public interface RegionCodeService {
    List<RegionCodes> getAll();

    RegionCodes findByName(String cityName);
}
