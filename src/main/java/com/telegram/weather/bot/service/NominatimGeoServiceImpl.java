package com.telegram.weather.bot.service;


import com.telegram.weather.bot.model.Address;
import com.telegram.weather.bot.model.GeoPoint;
import com.telegram.weather.bot.service.parser.ParseNominatim;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

@Slf4j
@Service
public class NominatimGeoServiceImpl implements NominatimGeoService {

    private static final String NOMINATIM_URI = "https://nominatim.openstreetmap.org";
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";

    @Override
    public Address getAddressByCoordinate(GeoPoint geoPoint) {

        HttpEntity httpEntity = prepareHttpEntity();

        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(NOMINATIM_URI).
                path("/reverse")
                .queryParam("format", "xml")
                .queryParam("lat", geoPoint.getLatitude())
                .queryParam("lon", geoPoint.getLongitude())
                .queryParam("zoom", "18")
                .queryParam("addressdetails", "1");

        ResponseEntity<String> stringResponseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, httpEntity, String.class);
        log.debug("Got response from reverse Nominatim service: " + stringResponseEntity.getStatusCode());

        Address address = ParseNominatim.parseXmlToAddress(Objects.requireNonNull(stringResponseEntity.getBody()));
        address.setGeoPoint(geoPoint);

        return address;
    }

    @Override
    public GeoPoint getCoordinateByAddress(Address address) {

        HttpEntity httpEntity = prepareHttpEntity();

        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(NOMINATIM_URI).
                path("/search")
                .queryParam("format", "xml")
                .queryParam("addressdetails", "1")
                .queryParam("limit", "1")
                .queryParam("polygon_svg", "1");

        String street = address.getRoad();
        if (!isStringNullOrEmpty(street)) {
            if (!isStringNullOrEmpty(address.getHouseNumber())) {
                street = street + " " + address.getHouseNumber();
            }
            builder.queryParam("street", street);
        }

        if (!isStringNullOrEmpty(address.getCity())) {
            builder.queryParam("city", address.getCity());
        }

        if (!isStringNullOrEmpty(address.getState())) {
            builder.queryParam("state", address.getState());
        }

        if (!isStringNullOrEmpty(address.getCountry())) {
            builder.queryParam("country", address.getCountry());
        }

        if (!isStringNullOrEmpty(address.getPostcode())) {
            builder.queryParam("postalcode", address.getPostcode());
        }

        ResponseEntity<String> stringResponseEntity = restTemplate.exchange(builder.encode().build().toUri(), HttpMethod.GET, httpEntity, String.class);
        log.debug("Got response from Nominatim service: " + stringResponseEntity.getStatusCode());

        return ParseNominatim.parseXmlToGeoPoint(Objects.requireNonNull(stringResponseEntity.getBody()));
    }

    private HttpEntity prepareHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("user-agent", USER_AGENT);
        headers.setContentType(MediaType.APPLICATION_XML);

        return new HttpEntity(headers);
    }

    private boolean isStringNullOrEmpty(final String str) {
        if (str != null && !str.trim().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}
