package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.Address;
import com.telegram.weather.bot.model.Trip;
import com.telegram.weather.bot.model.jakdojadequery.Coordinate;
import com.telegram.weather.bot.model.routes.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;


/**
 * https://developers.google.com/maps/documentation/maps-static/dev-guide
 */
@Slf4j
@Service
@PropertySource("classpath:telegram.properties")
public class GoogleMapStaticImpl implements MapService {

    private final static String STATIC_MAP_URL = "https://maps.googleapis.com/maps/api/staticmap";

    @Value("${google.api.key}")
    private String API_KEY;

    @Override
    public InputStream getImageByRoute(Trip lastTrip, Route route) {
        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(STATIC_MAP_URL)
                .queryParam("zoom", "12")
                .queryParam("size", "640x640")
                .queryParam("maptype", "roadmap")
                .queryParam("format", "gif")
                .queryParam("key", API_KEY);

        Address startPoint = lastTrip.getStartPoint();
        Address finishPoint = lastTrip.getEndPoint();
        urlBuilder.queryParam("markers", "color:green|label:S|" + startPoint.getGeoPoint().getLatitude() + "," + startPoint.getGeoPoint().getLongitude()) //Start marker
                .queryParam("markers", "label:F|" + finishPoint.getGeoPoint().getLatitude() + "," + finishPoint.getGeoPoint().getLongitude());//Finish marker


        for (RoutePart routePart : route.getRouteParts()) {
            RouteVehicle routeVehicle = routePart.getRouteVehicle();
            if (routeVehicle != null) {
                StringBuilder pathBuilder = new StringBuilder();
                pathBuilder.append("color:").append(getRandomColor()).append("|");
                pathBuilder.append("weight:5" + "|");
                for (RouteStop routeStop : routeVehicle.getRouteStops()) {
                    Coordinate stopCoordinate = routeStop.getLocation();
                    pathBuilder.append(stopCoordinate.getYLat()).append(",");
                    pathBuilder.append(stopCoordinate.getXLon()).append("|");
                }
                urlBuilder.queryParam("path", pathBuilder.toString().substring(0, pathBuilder.toString().length() - 1)); //path
            }
        }

        final String url = urlBuilder.toUriString();
        log.debug("GoogleStatic Map URL: " + url);

        try {
            URL urlAddress = new URL(url);

            HttpURLConnection httpClient = (HttpURLConnection) urlAddress.openConnection();
            httpClient.setRequestMethod("GET");

            /*
            BufferedImage bImage = ImageIO.read(httpClient.getInputStream());
            ImageIO.write(bImage, "jpg", new File("/Users/home/Downloads/image_http.jpg"));
             */

            return httpClient.getInputStream();

        } catch (IOException e) {
            log.error(e.getMessage());
        }

        throw new RuntimeException("Something went wrong during request to GoogleAPI");
    }

    public String getRandomColor() {
        Random rand = new Random();

        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
        int a = 255;

        int rR = (int) (r * 255 + 0.5);
        int gG = (int) (g * 255 + 0.5);
        int bB = (int) (b * 255 + 0.5);


        int value = ((a & 0xFF) << 24) |
                ((rR & 0xFF) << 16) |
                ((gG & 0xFF) << 8) |
                ((bB & 0xFF) << 0);
        return "0x" + Integer.toHexString(value).substring(2);
    }
}
