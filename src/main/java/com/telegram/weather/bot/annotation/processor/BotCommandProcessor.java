package com.telegram.weather.bot.annotation.processor;

import com.telegram.weather.bot.annotation.BotCommand;
import com.telegram.weather.bot.annotation.BotController;
import com.telegram.weather.bot.annotation.BotRequestParam;
import com.telegram.weather.bot.commands.BotParameters;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.service.Emoji;
import com.telegram.weather.bot.service.LocalisationService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

public class BotCommandProcessor implements InitializingBean, CommandProcessor {
    private static final String DEFAULT_METHOD = "default";
    private static final String COMMAND_PATTERN = "%s:%s";
    private final String packageName;

    private Map<String, String> controllers;


    @Autowired
    private ApplicationContext context;

    @Autowired
    private LocalisationService localisationService;

    public BotCommandProcessor(String packageName) {
        this.packageName = packageName;
    }


    @Deprecated
    private Map<String, String> findControllers(String packageName) {
        Map<String, String> controllers = new HashMap<>();

        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(true);
        scanner.addIncludeFilter(new AnnotationTypeFilter(BotController.class));

        for (BeanDefinition bd : scanner.findCandidateComponents(packageName)) {

            try {
                // Injecting dependencies and set beanName as value of BotController.value()
                Object createdBean = this.context.getAutowireCapableBeanFactory().createBean(Class.forName(bd.getBeanClassName()));

                String botControllerValue = createdBean.getClass().getDeclaredAnnotation(BotController.class).value();

                String beanName = createdBean.getClass().getCanonicalName();
                DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) this.context.getAutowireCapableBeanFactory();
                beanFactory.registerBeanDefinition(beanName, bd);

                //Retrieve all available method annotations for this class
                List<Method> methods = Arrays.stream(createdBean.getClass().getMethods()).filter(method -> {
                    Annotation annotation = method.getDeclaredAnnotation(BotCommand.class);
                    return annotation != null;
                }).collect(Collectors.toList());


                methods.forEach(method -> {
                    String commandName;
                    if (DEFAULT_METHOD.equals(method.getDeclaredAnnotation(BotCommand.class).name())) {
                        commandName = botControllerValue + method.getDeclaredAnnotation(BotCommand.class).name();
                    } else {
                        String[] emojis = Arrays.stream(method.getDeclaredAnnotation(BotCommand.class).emoji()).map(Emoji::toString).toArray(String[]::new);
                        commandName = botControllerValue + String.format(localisationService.getString(method.getDeclaredAnnotation(BotCommand.class).name()), emojis);
                    }
                    controllers.put(commandName, beanName);
                });

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        this.controllers = controllers;

        return controllers;
    }

    @Override
    public void discoverCommands() {
        this.controllers = new HashMap<>();

        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(true);
        scanner.addIncludeFilter(new AnnotationTypeFilter(BotController.class));

        for (BeanDefinition bd : scanner.findCandidateComponents(packageName)) {
            try {
                // Injecting dependencies and set beanName as value of BotController.value()
                Object createdBean = this.context.getAutowireCapableBeanFactory().createBean(Class.forName(bd.getBeanClassName()));

                String botControllerValue = createdBean.getClass().getDeclaredAnnotation(BotController.class).value();

                String beanName = createdBean.getClass().getCanonicalName();
                DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) this.context.getAutowireCapableBeanFactory();
                beanFactory.registerBeanDefinition(beanName, bd);

                //Retrieve all available method annotations for this class
                List<Method> methods = Arrays.stream(createdBean.getClass().getMethods()).filter(method -> {
                    Annotation annotation = method.getDeclaredAnnotation(BotCommand.class);
                    return annotation != null;
                }).collect(Collectors.toList());


                methods.forEach(method -> controllers.put(String.format(COMMAND_PATTERN, botControllerValue, method.getDeclaredAnnotation(BotCommand.class).command()), beanName));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Object executeCommand(final String state, final Update update, Object... args) {


        BotParameters botParameters = new BotParameters(update);
        Command receivedCommand = botParameters.getCommand();

        String invokedMethod = String.format(COMMAND_PATTERN, state, receivedCommand);

        String beanName = controllers.get(invokedMethod);
        if (beanName == null) {
            invokedMethod = state + Command.DEFAULT;
            beanName = controllers.get(invokedMethod);
        }
        if (beanName != null) {
            Object o = context.getBean(beanName);

            String finalInvokedMethod = invokedMethod;
            Method foundMethod = Arrays.stream(o.getClass().getMethods()).filter(method -> {
                Annotation annotation = method.getDeclaredAnnotation(BotCommand.class);
                if (annotation != null) {
                    String botController = o.getClass().getAnnotation(BotController.class).value();
                    Command invokedCommand = method.getDeclaredAnnotation(BotCommand.class).command();

                    return finalInvokedMethod.equals(String.format(COMMAND_PATTERN, botController, invokedCommand));
                } else {
                    return false;
                }
            }).findFirst().orElseThrow(RuntimeException::new);

            Object[] parsedParams = replaceMethodParameters(foundMethod, botParameters, args);
            return invokeMethod(o, foundMethod, parsedParams);
        }
        throw new RuntimeException("Command is not declared!");
    }

    private Object[] replaceMethodParameters(Method method, BotParameters botParameters, Object... incomingArgs) {
        Object[] arguments = new Object[method.getParameterCount()];
        Parameter[] parameters = method.getParameters();

        for (int i = 0; i < parameters.length; i++) {
            Annotation[] paramAnnotations = parameters[i].getDeclaredAnnotations();
            for (Annotation paramAnnotation : paramAnnotations) {
                if (BotRequestParam.class.isAssignableFrom(paramAnnotation.annotationType())) {
                    String requestParamName = parameters[i].getDeclaredAnnotation(BotRequestParam.class).name();
                    arguments[i] = botParameters.getParam(requestParamName);
                }
            }

            if (Update.class.isAssignableFrom(parameters[i].getType())) {
                for (Object inArg : incomingArgs) {
                    if (Update.class.isAssignableFrom(inArg.getClass())) {
                        arguments[i] = inArg;
                    }
                }
            } else if (Message.class.isAssignableFrom(parameters[i].getType())) {
                for (Object inArg : incomingArgs) {
                    if (Optional.ofNullable(inArg).isPresent() && Update.class.isAssignableFrom(inArg.getClass())) {
                        arguments[i] = ((Update) inArg).getMessage();
                    } else {
                        assert inArg != null;
                        if (Message.class.isAssignableFrom(inArg.getClass())) {
                            arguments[i] = inArg;
                        }
                    }
                }
            } else if (String.class.isAssignableFrom(parameters[i].getType()) && parameters[i].getName().equalsIgnoreCase("language")) {
                for (Object inArg : incomingArgs) {
                    if (!Optional.ofNullable(inArg).isPresent()) {
                        arguments[i] = "en";
                    } else if (String.class.isAssignableFrom(inArg.getClass())) {
                        arguments[i] = inArg;
                    }
                }
            }
        }

        return arguments;
    }


    @Deprecated
    public Object invoke(final String state, final String command, Object... args) {
        String invokedMethod = state + command;
        String beanName = controllers.get(invokedMethod);
        if (beanName == null) {
            invokedMethod = state + DEFAULT_METHOD;
            beanName = controllers.get(invokedMethod);
        }
        if (beanName != null) {
            Object o = context.getBean(beanName);

            String finalInvokedMethod = invokedMethod;
            Method foundMethod = Arrays.stream(o.getClass().getMethods()).filter(method -> {
                Annotation annotation = method.getDeclaredAnnotation(BotCommand.class);
                if (annotation != null) {
                    String botController = o.getClass().getAnnotation(BotController.class).value();
                    String localizedMethodName = localisationService.getString(method.getDeclaredAnnotation(BotCommand.class).name());
                    String[] emojis = Arrays.stream(method.getDeclaredAnnotation(BotCommand.class).emoji()).map(Emoji::toString).toArray(String[]::new);
                    String methodCommand = localizedMethodName == null ? DEFAULT_METHOD : String.format(localizedMethodName, emojis);
                    return finalInvokedMethod.equals(botController + methodCommand);
                } else {
                    return false;
                }
            }).findFirst().orElseThrow(RuntimeException::new);

            return invokeMethod(o, foundMethod, args);
        }
        throw new RuntimeException("Command not declared!");

    }

    private Object invokeMethod(Object obj, Method method, Object... arguments) {
        Parameter[] parameters = method.getParameters();
        if (parameters.length != arguments.length)
            throw new RuntimeException(String.format("Expected %d parameters, but got %d", parameters.length, arguments.length));
        for (int i = 0; i < parameters.length; i++) {
            if (!parameters[i].getType().equals(arguments[i].getClass())) {
                throw new RuntimeException("Method parameter is not equal. Expected: " + parameters[i].getType() + ", got: " + arguments[i].getClass());
            }
        }
        try {
            return method.invoke(obj, arguments);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        //this.controllers = findControllers(packageName);
        discoverCommands();
    }
}
