package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.Trip;
import com.telegram.weather.bot.model.routes.Route;

import java.io.InputStream;

public interface MapService {
    InputStream getImageByRoute(Trip lastTrip, Route route);
}
