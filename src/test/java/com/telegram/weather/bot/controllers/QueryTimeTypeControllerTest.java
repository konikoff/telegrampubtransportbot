package com.telegram.weather.bot.controllers;

import com.telegram.weather.bot.model.Address;
import com.telegram.weather.bot.model.Trip;
import com.telegram.weather.bot.model.routes.Route;
import com.telegram.weather.bot.model.routes.Routes;
import com.telegram.weather.bot.service.parser.ResourceFile;
import com.telegram.weather.bot.utils.ParseUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Locale;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class QueryTimeTypeControllerTest {

    private static final String TEMPLATE_ENCODING = "UTF-8";
    private static final String TEMPLATES_TXT = "/templates/txt/";

    private Trip lastTrip;

    @Rule
    public ResourceFile res = new ResourceFile("/routes.json");

    @Test
    public void defaultCommand() {
        String timeUnformatted = "22:59";

        LocalTime localDate = LocalTime.from(DateTimeFormatter.ISO_TIME.parse(timeUnformatted));
        Instant instantFromTime = localDate.atDate(LocalDate.now()).atZone(ZoneId.of("Europe/Warsaw")).toInstant();

        System.out.println(instantFromTime);
        assertNotNull(instantFromTime);

        String dateTimeUnformatted = "01-01 22:59";

        DateTimeFormatter parseFormatter = new DateTimeFormatterBuilder()
                .appendPattern("dd-MM HH:mm")
                .parseDefaulting(ChronoField.YEAR, LocalDate.now().getYear())
                .toFormatter(Locale.ENGLISH);

        LocalDateTime localDateTime = LocalDateTime.from(parseFormatter.parse(dateTimeUnformatted));

        Instant instantFromDateTime = localDateTime.atZone(ZoneId.of("Europe/Warsaw")).toInstant();
        System.out.println(instantFromDateTime);
    }

    @Before
    public void setLastTrip() {
        lastTrip = new Trip();

        lastTrip.setStartPoint(new Address() {{
                                   setRoad("Artemovskaia");
                                   setHouseNumber("55C");
                                   setCity("Kryzopol");
                               }}
        );

        lastTrip.setEndPoint(new Address() {{
                                   setRoad("Artemovskaia");
                                   setHouseNumber("55C");
                                   setCity("Kryzopol");
                               }}
        );
    }

    @Test
    public void getTripMessage() throws IOException {
        final Context ctx = new Context();

        Routes routes = ParseUtils.parseRoutes(res.getContent());

        for (Route route : routes.getRoutes()) {
            ctx.setVariable("route", route);
            ctx.setVariable("trip", lastTrip);
            String outMessageText = botTemplateEngine().process("route/routes_message", ctx);

            System.out.println(outMessageText);
        }
    }

    private TemplateEngine botTemplateEngine() {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        // Resolver for TEXT emails
        templateEngine.addTemplateResolver(textTemplateResolver());
        return templateEngine;
    }

    private ITemplateResolver textTemplateResolver() {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix(TEMPLATES_TXT);
        templateResolver.setSuffix(".txt");
        templateResolver.setTemplateMode(TemplateMode.TEXT);
        templateResolver.setCharacterEncoding(TEMPLATE_ENCODING);
        templateResolver.setCharacterEncoding("UTF8");
        templateResolver.setCheckExistence(true);
        templateResolver.setCacheable(false);
        return templateResolver;
    }
}
