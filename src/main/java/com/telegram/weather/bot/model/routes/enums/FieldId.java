package com.telegram.weather.bot.model.routes.enums;

public enum FieldId {
    CONTACT_PHONE_NUMBER, LINE_NAME, VEHICLE_TYPE, AUTHORITY_SYMBOL
}
