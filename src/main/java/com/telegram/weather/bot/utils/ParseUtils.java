package com.telegram.weather.bot.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.telegram.weather.bot.model.addresses.Addresses;
import com.telegram.weather.bot.model.routes.Routes;
import com.telegram.weather.bot.model.routes.adapters.InstantDeserializer;
import com.telegram.weather.bot.model.routes.adapters.InstantSerializer;
import com.telegram.weather.bot.model.jakdojadequery.JakdojadeQuery;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;

@Slf4j
public class ParseUtils {

    public static Routes parseRoutes(String routesJson) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Instant.class, new InstantDeserializer());

        Gson gson = gsonBuilder.create();
        log.debug("Responded JSON: " + routesJson);
        Routes routes = gson.fromJson(routesJson, Routes.class);

        log.debug("Retrieved " + routes.getRoutes().size() + " routes");

        return routes;
    }

    public static String jakdojadeQueryToJson(JakdojadeQuery jakdojadeQuery) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Instant.class, new InstantSerializer());
        String preparedJson = gsonBuilder.serializeNulls().create().toJson(jakdojadeQuery);
        log.debug("JakdojadeQuery: " + preparedJson);
        return preparedJson;
    }

    public static Addresses parseAddresses(String addressesJson) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        Addresses addresses = gson.fromJson(addressesJson, Addresses.class);

        return addresses;
    }
}
