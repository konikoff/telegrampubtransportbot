package com.telegram.weather.bot.model;

public class StateCommand {
    public static final String DEFAULT = "DEFAULT";
    public static final String START_STATE = "START_STATE";
    public static final String MAIN_MENU = "MAIN_MENU";
    public static final String SETTINGS_LANGUAGE = "SETTINGS_LANGUAGE";
    public static final String SETTINGS = "SETTINGS";
    public static final String SETTINGS_CITY = "SETTINGS_CITY";
    public static final String TRIP = "TRIP";
    public static final String TRIP_LOCATION_END = "TRIP_LOCATION_END";
    public static final String TRIP_TIME_TYPE = "TRIP_TIME_TYPE";
    public static final String TRIP_LOCATION_START = "TRIP_LOCATION_START";
    public static final String CHOOSE_ROUTE = "CHOOSE_ROUTE";
}
