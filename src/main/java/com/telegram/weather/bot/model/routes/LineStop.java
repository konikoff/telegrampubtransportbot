package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.BoardingRestriction;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LineStop {
    private StopPoint stopPoint;
    private String lineStopDynamicId;
    private Boolean onDemand;
    private Integer stopZoneCode;
    private String stopZoneName;
    private BoardingRestriction boardingRestriction;
}
