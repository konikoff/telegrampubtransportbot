package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.FieldId;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Parameter {
    private FieldId fieldId;
    private String displayName;
    private String displayHint;
    private Boolean technical;
    private Boolean purchaseParameter;
}
