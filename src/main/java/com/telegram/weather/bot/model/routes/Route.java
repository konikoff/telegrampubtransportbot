package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.RouteType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class Route {
    private String routeId;
    private RouteType type;
    private List<RoutePart> routeParts;
    private RealtimeInformation realtimeInformation;
    private Boolean alternativeRoute;
    private RouteFare routeFare;
    private String routeDescription;
    private String routeLink;
}
