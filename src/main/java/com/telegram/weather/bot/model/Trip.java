package com.telegram.weather.bot.model;

import com.telegram.weather.bot.model.routes.enums.QueryTimeType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "Trip")
public class Trip extends BaseEntity {

    @OneToOne(cascade = CascadeType.ALL)
    private Address startPoint;

    @OneToOne(cascade = CascadeType.ALL)
    private Address endPoint;

    private QueryTimeType queryTimeType;

    @Column(name = "timestamp")
    private Instant timestamp;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}
