package com.telegram.weather.bot.service.parser;

import com.google.gson.*;
import com.telegram.weather.bot.model.RegionCodes;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.Set;

@Slf4j
public class ParseRegions {

    public static Set<RegionCodes> unmarshalJsonRoutes(String routeJson) {
        Set<RegionCodes> regionCodes = new HashSet<>();

        Gson gson = new GsonBuilder().create();

        JsonObject regionsJson = gson.fromJson(routeJson, JsonObject.class);

        JsonArray regionsArray = regionsJson.getAsJsonArray("regions");
        log.debug("Got regionCodes: " + regionsArray.size());
        for (JsonElement routeElement : regionsArray) {

            JsonObject routeObject = routeElement.getAsJsonObject();
            String regionSymbol = routeObject.get("regionSymbol").getAsString();

            JsonArray citiesArray = routeObject.getAsJsonArray("cities");
            for (JsonElement cityElement : citiesArray) {
                JsonObject cityObject = cityElement.getAsJsonObject();

                RegionCodes regionCode = new RegionCodes();

                regionCode.setCityName(cityObject.get("cityName").getAsString());
                regionCode.setRegionSymbol(regionSymbol);
                regionCodes.add(regionCode);
            }
        }
        return regionCodes;
    }
}
