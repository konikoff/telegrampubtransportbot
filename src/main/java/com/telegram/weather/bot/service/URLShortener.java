package com.telegram.weather.bot.service;

import java.io.IOException;

public interface URLShortener {

    String getShortenedURL(String fullURL) throws IOException;

}
