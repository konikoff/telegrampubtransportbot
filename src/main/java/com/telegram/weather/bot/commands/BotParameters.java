package com.telegram.weather.bot.commands;

import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class BotParameters {

    private Command command;
    private Map<String, String> botParameters = new HashMap<>();

    public BotParameters() {
    }

    public BotParameters(Update update) {
        extractParams(update);
    }

    private void extractParams(Update update) {
        // if missed a callback query use default command
        this.command = Command.DEFAULT;
        if (update.getCallbackQuery() != null && update.getCallbackQuery().getData() != null) {
            final String callbackData = update.getCallbackQuery().getData();

            String[] data = callbackData.split(":");
            for (String param : data) {
                try {
                    this.command = Enum.valueOf(Command.class, param);
                } catch (IllegalArgumentException e) {
                    String[] params = param.split("=");
                    botParameters.put(params[0], params[1]);
                }
            }
        }
    }

    public String getParam(String name) {
        return botParameters.getOrDefault(name, "");
    }

    public BotParameters setParam(String name, String value) {
        botParameters.put(name, value);

        return this;
    }

    public BotParameters setCommand(Command command) {
        this.command = command;

        return this;
    }

    public Command getCommand() {
        return this.command;
    }

    public String getParamQuery() {
        AtomicReference<String> paramQuery = new AtomicReference<>(this.command.name());
        botParameters.forEach((name, value) -> paramQuery.set(paramQuery + ":" + name + "=" + value));

        return paramQuery.get();
    }

    @Override
    public String toString() {
        return getParamQuery();
    }
}
