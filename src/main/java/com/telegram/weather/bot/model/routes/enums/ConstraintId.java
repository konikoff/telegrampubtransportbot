package com.telegram.weather.bot.model.routes.enums;

public enum ConstraintId {
    LINE_TYPES_CONSTRAINT, LINES_COUNT_LIMIT
}
