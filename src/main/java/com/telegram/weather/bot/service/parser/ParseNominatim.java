package com.telegram.weather.bot.service.parser;

import com.telegram.weather.bot.model.Address;
import com.telegram.weather.bot.model.GeoPoint;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class ParseNominatim {
    private final static Logger logger = LoggerFactory.getLogger(ParseNominatim.class);

    public static Address parseXmlToAddress(@NotNull String document) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            InputStream streamXmlDoc = new ByteArrayInputStream(document.getBytes(StandardCharsets.UTF_8));

            Document doc = dBuilder.parse(streamXmlDoc);

            Address address = new Address();

            NodeList addressPartsList = doc.getElementsByTagName("addressparts");
            for (int i = 0; i < addressPartsList.getLength(); i++) {
                Node node = addressPartsList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    String houseNumber = getStringFromNode(element.getElementsByTagName("house_number").item(0));
                    address.setHouseNumber(houseNumber);

                    String road = getStringFromNode(element.getElementsByTagName("road").item(0));
                    address.setRoad(road);

                    String neighbourhood = getStringFromNode(element.getElementsByTagName("neighbourhood").item(0));
                    address.setNeighbourhood(neighbourhood);

                    String suburb = getStringFromNode(element.getElementsByTagName("suburb").item(0));
                    address.setSuburb(suburb);

                    String city = getStringFromNode(element.getElementsByTagName("city").item(0));
                    address.setCity(city);

                    String county = getStringFromNode(element.getElementsByTagName("county").item(0));
                    address.setCounty(county);

                    String country = getStringFromNode(element.getElementsByTagName("country").item(0));
                    address.setCountry(country);

                    String state = getStringFromNode(element.getElementsByTagName("state").item(0));
                    address.setState(state);

                    String postcode = getStringFromNode(element.getElementsByTagName("postcode").item(0));
                    address.setPostcode(postcode);
                }
            }

            return address;


        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.error("Occurred exception during parsing XML.", e.getCause());
        }

        throw new RuntimeException("Inbound XML wasn't parsed.");
    }

    public static GeoPoint parseXmlToGeoPoint(String document) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            InputStream streamXmlDoc = new ByteArrayInputStream(document.getBytes(StandardCharsets.UTF_8));

            Document doc = dBuilder.parse(streamXmlDoc);

            float lat = 0.0f;
            float lon = 0.0f;

            NodeList addressPartsList = doc.getElementsByTagName("place");
            for (int i = 0; i < addressPartsList.getLength(); i++) {
                Node node = addressPartsList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    lat = Float.parseFloat(element.getAttribute("lat"));
                    lon = Float.parseFloat(element.getAttribute("lon"));
                }
            }
            return new GeoPoint(lon, lat);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.error("Occurred exception during parsing XML.", e.getCause());
        }

        throw new RuntimeException("Inbound XML wasn't parsed.");
    }

    @Contract("null -> null")
    private static String getStringFromNode(Node node) {
        if (node != null) {
            return node.getTextContent();
        } else {
            return null;
        }
    }
}
