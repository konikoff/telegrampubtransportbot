package com.telegram.weather.bot.utils;

import com.telegram.weather.bot.model.JakdojadeUser;
import org.junit.Test;

import static org.junit.Assert.*;

public class SignatureTest {

    @Test
    public void extractURLPath() {
        JakdojadeUser jakdojadeUser = new JakdojadeUser();
        jakdojadeUser.setPasswordHash("");
        jakdojadeUser.setUserDeviceId("");
        jakdojadeUser.setProfileLogin("");
        Signature signature = new Signature("https://jakdojade.pl/api/web/v1/regionDetails?citySymbol=GDANSK", jakdojadeUser, null);

        assertEquals("/api/web/v1/regionDetails", signature.getApiURLPath());

        Signature signature2 = new Signature("https://jakdojade.pl/api/web/v1/regionDetails", jakdojadeUser, null);
        assertEquals("/api/web/v1/regionDetails", signature2.getApiURLPath());
    }

    @Test
    public void extractParameters() {
        JakdojadeUser jakdojadeUser = new JakdojadeUser();
        jakdojadeUser.setPasswordHash("");
        jakdojadeUser.setUserDeviceId("");
        jakdojadeUser.setProfileLogin("");
        Signature signature = new Signature("https://jakdojade.pl/api/web/v1/regionDetails?citySymbol=GDANSK", jakdojadeUser, null);
        assertEquals("citySymbol=GDANSK", signature.getQueryParams());
    }

    @Test
    public void testAddressUrl() {
        JakdojadeUser jakdojadeUser = new JakdojadeUser();
        jakdojadeUser.setPasswordHash("");
        jakdojadeUser.setUserDeviceId("");
        jakdojadeUser.setProfileLogin("");
        Signature signature = new Signature(" https://jakdojade.pl/api/web/v1/addresses?location=54.4063304:18.5957872", jakdojadeUser, null);
        assertEquals("location=54.4063304%3A18.5957872", signature.getQueryParams());
    }

}
