package com.telegram.weather.bot.configs;

import com.telegram.weather.bot.annotation.processor.BotCommandProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ApplicationConfig {

    @Bean
    public BotCommandProcessor botCommandProcessor() {
        return new BotCommandProcessor("com.telegram.weather.bot.controllers");
    }
}
