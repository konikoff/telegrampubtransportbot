package com.telegram.weather.bot.model.repositories;

import com.telegram.weather.bot.model.RegionCodes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RegionRepository extends JpaRepository<RegionCodes, Long> {

    @Query(value = "select r from RegionCodes r where r.cityName=:cityName and r.regionSymbol=:regionSymbol")
    RegionCodes findRegionByCityNameAndRegionSymbol(@Param("cityName") String cityName, @Param("regionSymbol") String regionSymbol);

    @Query("FROM RegionCodes WHERE cityName=:cityName")
    Optional<RegionCodes> findOneByName(@Param("cityName") String cityName);

}

