package com.telegram.weather.bot.converters;

import com.telegram.weather.bot.model.Address;
import com.telegram.weather.bot.model.GeoPoint;
import com.telegram.weather.bot.model.addresses.Addresses;
import com.telegram.weather.bot.model.jakdojadequery.Location;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class AddressesToAddressConverter implements Converter<Addresses, Address> {
    @Override
    public Address convert(Addresses addresses) {
        Address address = new Address();
        if (!addresses.getLocations().isEmpty()) {
            Location location = addresses.getLocations().get(0);
            address.setCity(location.getAddress().getAddressCity());
            address.setHouseNumber(location.getAddress().getAddressNumber());
            address.setRoad(location.getAddress().getAddressStreet());

            GeoPoint geoPoint = new GeoPoint();
            geoPoint.setLatitude(Float.valueOf(location.getCoordinate().getYLat()));
            geoPoint.setLongitude(Float.valueOf(location.getCoordinate().getXLon()));

            address.setGeoPoint(geoPoint);

            address.setSuburb(location.getSubName());
            address.setNeighbourhood(location.getAddress().getAddressDistrict());

            return address;
        } else {
            throw new RuntimeException("Retrieved address doesn't contain any data!");
        }
    }
}
