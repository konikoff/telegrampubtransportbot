package com.telegram.weather.bot.model.jakdojadequery;

import com.telegram.weather.bot.model.jakdojadequery.enums.FetchType;
import com.telegram.weather.bot.model.jakdojadequery.enums.RoutesCorrelation;

import com.telegram.weather.bot.model.routes.SearchQuery;
import lombok.*;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JakdojadeQuery {
    private FetchType fetchType;
    private RoutesCorrelation routesCorrelation;
    private SearchQuery searchQuery;
    private Location userLocation;
}
