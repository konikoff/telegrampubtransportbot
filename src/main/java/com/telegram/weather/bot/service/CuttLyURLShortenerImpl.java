package com.telegram.weather.bot.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Slf4j
@Service
public class CuttLyURLShortenerImpl implements URLShortener {

    private static final String CUTT_LY_URL = "https://cutt.ly/api/api.php?key=%s&short=%s";
    private static final String API_KEY = "df2978f63423bbc5c7453b6af640ad5df7085";

    @Override
    public String getShortenedURL(String fullURL) {
        try {
            URL url = new URL(String.format(CUTT_LY_URL, API_KEY, fullURL));
            HttpURLConnection httpClient = (HttpURLConnection) url.openConnection();
            httpClient.setRequestMethod("GET");

            try (BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()))) {

                StringBuilder response = new StringBuilder();
                String line;

                while ((line = in.readLine()) != null) {
                    response.append(line);
                }

                Gson gson = new GsonBuilder().create();

                JsonObject shortenedResponseJson = gson.fromJson(response.toString(), JsonObject.class);

                return shortenedResponseJson.getAsJsonObject("url").getAsJsonPrimitive("shortLink").toString();
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        throw new RuntimeException("Something went wrong with parsing Shortened URL!");
    }
}
