package com.telegram.weather.bot.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.ApiContext;

@Slf4j
public class SendMessageExecutor {

    @Value("${telegram.bot.token}")
    private String token;

    private final DefaultBotOptions options;
    private final DefaultAbsSender sender;

    public SendMessageExecutor() {
        this.options = ApiContext.getInstance(DefaultBotOptions.class);
        this.sender = new DefaultAbsSender(options) {

            @Override
            public String getBotToken() {
                return token;
            }
        };
    }

    protected DefaultAbsSender getSender() {
        return sender;
    }
}
