package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.VehicleType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class RouteVehicle {
    private VehicleType routeVehicleType;
    private List<RouteStop> routeStops;
    private Integer stopsStartIndex;
    private Integer stopsEndIndex;
    private RouteLine routeLine;
}
