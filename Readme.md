# Public Transport Chat Bot for Telegram

This chat bot temporarily available under this link  - [GrizliWheaterBot](https://t.me/grizliwheaterbot).

Currently available the following possibilities:
* Sending start and destination location points as coordinates and receive a response message with possible routes between them.