package com.telegram.weather.bot.configs.interceptors;

import com.telegram.weather.bot.utils.JakdojadeUtils;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class RequestInterceptorTest {

    @Test
    public void computeUserDeviceId() {
        System.out.println(JakdojadeUtils.computeUserDeviceId());

        //(^[0-9]{13}_[0].[0-9]{17}_jakdojade)
        assertEquals("",JakdojadeUtils.computeUserDeviceId());
    }

}
