package com.telegram.weather.bot.commands;

public enum Command {
    DEFAULT,
    NEW_TRIP,
    SETTINGS,
    NOW,
    ARRIVAL,
    DEPARTURE,
    CANCEL,
    CITY_SETTINGS,
    UPDATE_CITY,
    UPDATE_LANGUAGE,
    LANGUAGE_SETTINGS,
    BACK;
}
