package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface ChatService {
    /**
     * Load last saved chat
     *
     * @param message
     * @return
     */
    Chat getChat(Message message);

    /**
     * Update status of the chat
     *
     * @param message
     * @param state
     * @return
     */
    Chat updateStatus(Message message, String state);
}
