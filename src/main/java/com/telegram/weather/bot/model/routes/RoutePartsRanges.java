package com.telegram.weather.bot.model.routes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoutePartsRanges {
    private StopIndex fromStopIndex;
    private StopIndex toStopIndex;
}
