Your trip:
 from 🚩 [(${trip.startPoint.road})] [(${trip.startPoint.houseNumber})], [(${trip.startPoint.city})]
 to 🏁 [(${trip.endPoint.road})] [(${trip.endPoint.houseNumber})], [(${trip.endPoint.city})].

[# th:each="routePart,iterRoutePart : ${route.getRouteParts()}" ]
    [# th:each="routeStop,iterStop : ${routePart?.getRouteVehicle()?.getRouteStops()}" ]
        [(${iterStop.count})]. [(${routeStop?.lineStop.stopPoint.stopName})]
    [/]

    [# th:if="${iterRoutePart.count > 2}"] 🔄 [/]
[/]


