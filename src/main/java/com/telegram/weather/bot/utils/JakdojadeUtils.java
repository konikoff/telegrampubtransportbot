package com.telegram.weather.bot.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Date;

@Slf4j
public class JakdojadeUtils {

    public static final String URI = "https://jakdojade.pl";
    public static final String REGISTER_ANONYMOUS = "/api/profiles/v1/register-anonymous";
    public static final String REGIONS = "/api/web/v1/regions";
    public static final String ADDRESSES = "/api/web/v1/addresses";
    public static final String ROUTES = "/api/jd/v1/routes";

    public static final String USER_DEVICE_ID = "X-jd-param-user-device-id";
    public static final String PARAM_PROFILE_LOGIN = "X-jd-param-profile-login";
    public static final String SECURITY_VERSION = "X-jd-security-version";
    public static final String SIGN = "X-jd-sign";
    public static final String TIMESTAMP = "X-jd-timestamp";
    public static final String PARAM_LOCALE = "X-jd-param-locale";
    public static final String PARAM_APPV = "X-jd-param-appV";

    public static final String SECURITY_VERSION_VALUE = "2";

    /*public static String getTimestamp() {
        return String.format("%10s", new Date().getTime());
    }*/

    /**
     * Generate X-jd-param-user-device-id:     1570535946101_0.4797331312393456_jakdojade
     * 1570618815884_0.7827501930979192_jakdojade
     * return e || (e = (new Date).getTime() + "_" + Math.random() + "_jakdojade", r(o.deviceId, e)), e
     */

    public static String computeUserDeviceId() {
        return String.format("%s_%s_jakdojade", new Date().getTime(), Math.random());
    }

    public static String computeSign(Signature signature) {
        final String base64Params = JakdojadeUtils.encryptBase64(DigestUtils.md5(signature.getQueryParams()));
        final String timestamp = signature.getTimestamp();
        final String profileLogin = signature.getProfileLogin();
        final String urlPath = signature.getApiURLPath();
        final String plainSign = urlPath.toLowerCase() + "_" + timestamp + "_" + profileLogin + "_" + signature.getM() + "_" + base64Params;

        final String profileHash = signature.getPasswordHash();
        return JakdojadeUtils.encryptBase64(JakdojadeUtils.hmacSHA512(plainSign, profileHash));
    }

    //return CryptoJS.enc.Base64.stringify(e).replace(/\+/g, "-").replace(/\//g, "_").replace(/=/g, "")
    private static String encryptBase64(byte[] data) {
        return Base64.getUrlEncoder().encodeToString(data)
                .replace("+", "_")
                .replace("\\", "_")
                .replace("=", "");
    }

    /**
     * @param data
     * @param key  - JakdojadeUser.passwordHash
     * @return
     */
    private static byte[] hmacSHA512(String data, String key) {
        try {
            final SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "HmacSHA512");

            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(secretKey);
            return mac.doFinal(data.getBytes());
        } catch (final NoSuchAlgorithmException | InvalidKeyException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static int getTimestamp() {
        return Math.toIntExact(new Date().getTime() / 1000);
    }


}
