package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.AccessibilityOptions;
import com.telegram.weather.bot.model.routes.enums.AvoidChanges;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TransportOptions {
    private AvoidChanges avoidChanges;
    private AccessibilityOptions accessibilityOptions;
}
