package com.telegram.weather.bot.model.repositories;

import com.telegram.weather.bot.model.Trip;
import com.telegram.weather.bot.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TripRepository extends JpaRepository<Trip, Long> {

    @Query("select t from Trip t where t.user=:user and t.endPoint is empty order by t.id desc")
    Optional<List<Trip>> findTripByUserWhereEndpointIsEmpty(@Param("user") User user);

    @Query("select t from Trip t where t.user=:user order by t.id desc")
    Optional<List<Trip>> findTripsByUser(@Param("user") User user);
}
