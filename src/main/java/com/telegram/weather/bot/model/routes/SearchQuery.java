package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.ConnectionTypePreference;
import com.telegram.weather.bot.model.routes.enums.RouteType;
import com.telegram.weather.bot.model.routes.enums.SearchMode;
import lombok.*;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchQuery {
    private Point start;
    private Point destination;
    private TimeOptions timeOptions;
    private RouteType[] allowedRouteTypes;
    private SearchMode realtimeSearchMode;
    private ConnectionTypePreference userConnectionTypePreference;
    private TransportOptions publicTransportOptions;
    private Integer routesCount;
}
