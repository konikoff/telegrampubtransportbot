package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.VehicleType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class Line {
    private String lineId;
    private String name;
    private VehicleType vehicleType;
    private List<Object> lineTypes;


}
