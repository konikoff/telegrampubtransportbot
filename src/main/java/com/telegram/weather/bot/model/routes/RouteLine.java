package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.RealtimeStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class RouteLine {
    private Line line;
    private String lineHeadingText;
    private TransportOperator transportOperator;
    private Boolean notMainVariant;
    private Integer departuresPeriodMinutes;
    private Boolean courseLine;
    private String courseId;
    private RealtimeStatus realtimeStatus;
    private List<Object> alternativeLines; //FIXME: required to check
}
