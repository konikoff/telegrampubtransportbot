package com.telegram.weather.bot.utils;

import com.telegram.weather.bot.model.JakdojadeUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Signature {

    private String apiURLPath;
    private String timestamp;
    private String m = ""; //FIXME: required additional checks
    private String queryParams = "";
    private String profileLogin;
    private String passwordHash;

    // Example: https://jakdojade.pl/api/web/v1/regionDetails?citySymbol=GDANSK
    public Signature(String URL, JakdojadeUser jakdojadeUser, String timestamp) {
        this.apiURLPath = extractUrlPath(URL);
        this.timestamp = timestamp;
        this.queryParams = extractQueryParams(URL);
        this.profileLogin = jakdojadeUser.getProfileLogin();
        this.passwordHash = jakdojadeUser.getPasswordHash();
    }

    private String extractUrlPath(final String URL) {
        int startPos = URL.indexOf("/", URL.indexOf("//") + 2);
        int endPos = URL.indexOf("?");

        return URL.substring(startPos, endPos != -1 ? endPos : URL.length());
    }

    /*
    p.join('&')
    .replace(/:/g, '%3A')
    .replace(/\%20/g, '+')
    .replace(/ /g, '+')
    .replace(/\(/g, '%28')
    .replace(/\)/g, '%29')
    .replace(/'/g, '%27')
    .replace(/,/g, '%2C')
    .replace(/;/g, '%3B')
    .split('&').sort().join('&'))
     */
    private String extractQueryParams(final String URL) {
        int startPos = URL.indexOf("?") + 1;

        return URL.substring(startPos)
                .replace(":", "%3A");
    }
}
