package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.JakdojadeUser;

public interface JakdojadeUserService {

    boolean isRegistered(Chat chat);

    boolean register(JakdojadeUser jakdojadeUser);

    JakdojadeUser getJakdojadeUserByChat(Chat chat);
}
