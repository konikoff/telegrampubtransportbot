package com.telegram.weather.bot.utils;

import com.telegram.weather.bot.model.JakdojadeUser;
import org.junit.Test;

import static org.junit.Assert.*;

public class JakdojadeUtilsTest {

    @Test
    public void computeSign() {

        String URL = "https://jakdojade.pl/api/web/v1/regionDetails?citySymbol=GDANSK";
        JakdojadeUser jakdojadeUser = new JakdojadeUser();
        jakdojadeUser.setProfileLogin("jda-2c6d5b93-a06b-4a48-b9d3-2c9203c1d02djjQ16db4c7Fx2eb20ea6");
        jakdojadeUser.setUserDeviceId("1570802559740_0.4987721000071679_jakdojade");
        jakdojadeUser.setPasswordHash("FSPD5VFMgCxZeZ2iO4HlvIsavbeJR1_t3XMSIlb7XbuM9z21rLO0pYSsf9lope4uFuL-owFMVXCmFDCVkI0wuA");
        Signature signature = new Signature(URL, jakdojadeUser, "1571210274");
        final String computedSign = JakdojadeUtils.computeSign(signature);
        assertEquals("7lhVUe83lwvp3oMGVMMYh5grFtOwD4rOHwh5pAhmoWg1-jbws0HH-OKZdQgZLH7seWk6_49ck9xOHEhF1ZVZTQ", computedSign);
    }
}