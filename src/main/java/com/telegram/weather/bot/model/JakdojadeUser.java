package com.telegram.weather.bot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "JakdojadeUser")
public class JakdojadeUser extends BaseEntity {

    @Column(name = "profileLogin")
    private String profileLogin;

    @Column(name = "passwordHash")
    private String passwordHash;

    @Column(name = "userDeviceId")
    private String userDeviceId;

    @OneToOne
    private Chat chat;
}
