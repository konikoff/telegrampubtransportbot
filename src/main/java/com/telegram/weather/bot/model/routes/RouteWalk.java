package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.jakdojadequery.Coordinate;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@ToString
public class RouteWalk {
    private List<Coordinate> walkShape;
    private Instant arrivalAtDestination;
}
