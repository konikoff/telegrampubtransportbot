package com.telegram.weather.bot.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Each BotController must contains a method with annotation @BotCommand(name = "default")
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface BotController {

    /**
     * Set controller name
     *
     * @return
     */
    String value() default "";
}
