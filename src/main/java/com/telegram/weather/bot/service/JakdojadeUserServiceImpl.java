package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.JakdojadeUser;
import com.telegram.weather.bot.model.repositories.JakdojadeUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class JakdojadeUserServiceImpl implements JakdojadeUserService {
    private final JakdojadeUserRepository jakdojadeUserRepository;

    public JakdojadeUserServiceImpl(JakdojadeUserRepository jakdojadeUserRepository) {
        this.jakdojadeUserRepository = jakdojadeUserRepository;
    }

    @Override
    public boolean isRegistered(Chat chat) {
        return this.jakdojadeUserRepository.findJakdojadeUserByChat(chat).isPresent();
    }

    @Override
    public boolean register(JakdojadeUser jakdojadeUser) {
        return this.jakdojadeUserRepository.save(jakdojadeUser) != null;
    }

    @Override
    public JakdojadeUser getJakdojadeUserByChat(Chat chat) {
        return this.jakdojadeUserRepository.findJakdojadeUserByChat(chat).orElseThrow(RuntimeException::new);
    }
}
