package com.telegram.weather.bot.model.repositories;

import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.JakdojadeUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JakdojadeUserRepository extends JpaRepository<JakdojadeUser, Long> {

    Optional<JakdojadeUser> findJakdojadeUserByChat(Chat chat);
}
