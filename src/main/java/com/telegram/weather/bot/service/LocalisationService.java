package com.telegram.weather.bot.service;

import java.util.List;

public interface LocalisationService {
    String getString(String key);

    String getString(String key, String language);

    List<LocalisationServiceImpl.Language> getSupportedLanguages();

    LocalisationServiceImpl.Language getLanguageByCode(String languageCode);

    LocalisationServiceImpl.Language getLanguageByName(String languageName);

    String getLanguageCodeByName(String language);
}
