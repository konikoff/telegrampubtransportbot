package com.telegram.weather.bot.service;

import com.telegram.weather.bot.commands.RouteCommand;
import com.telegram.weather.bot.model.Address;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.GeoPoint;
import com.telegram.weather.bot.model.routes.Routes;

public interface JakdojadeService {

    /**
     * POST - https://jakdojade.pl/api/profiles/v1/register-anonymous
     */
    void registerAnonymous(Chat chat);

    void fetchAllRegions(Chat chat);

    Routes getRoutes(RouteCommand routeCommand);

    Address getAddressByCoordinate(Chat chat, GeoPoint geoPoint);
}
