package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.jakdojadequery.Coordinate;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Point {
    private String regionSymbol;
    private Coordinate coordinate;
}
