package com.telegram.weather.bot.service;

import com.telegram.weather.bot.model.User;

public interface UserService {
    boolean save(User user);
}
