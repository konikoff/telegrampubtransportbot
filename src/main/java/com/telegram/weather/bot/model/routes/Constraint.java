package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.ConstraintId;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class Constraint {
    private ConstraintId constraintId;
    private String name;
    private Boolean visible;
    private List<Object> lineTypes; //FIXME: require additional checking
    private Integer linesCount;
}
