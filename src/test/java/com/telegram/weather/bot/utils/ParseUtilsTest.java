package com.telegram.weather.bot.utils;

import com.telegram.weather.bot.model.addresses.Addresses;
import com.telegram.weather.bot.model.routes.Point;
import com.telegram.weather.bot.model.routes.Routes;
import com.telegram.weather.bot.model.routes.SearchQuery;
import com.telegram.weather.bot.model.routes.enums.ConnectionTypePreference;
import com.telegram.weather.bot.model.routes.enums.SearchMode;
import com.telegram.weather.bot.model.jakdojadequery.Coordinate;
import com.telegram.weather.bot.model.jakdojadequery.JakdojadeQuery;
import com.telegram.weather.bot.model.jakdojadequery.enums.FetchType;
import com.telegram.weather.bot.model.jakdojadequery.enums.RoutesCorrelation;
import com.telegram.weather.bot.service.parser.ResourceFile;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.time.Instant;

import static org.junit.Assert.*;

public class ParseUtilsTest {

    @Rule
    public ResourceFile routesRes = new ResourceFile("/routes.json");
    public ResourceFile addressRes = new ResourceFile("/address.json");

    @Test
    public void parseRoutes() throws IOException {
        Routes routes = ParseUtils.parseRoutes(routesRes.getContent());

        assertNotNull(routes);

        System.out.println(routes.toString());
    }

    @Test
    public void objectToJsonSearchQuery() {
        Coordinate startCoordinate = new Coordinate();
        startCoordinate.setXLon("18.574448");
        startCoordinate.setYLat("54.426021");

        Coordinate destinationCoordinate = new Coordinate();
        destinationCoordinate.setXLon("18.592716");
        destinationCoordinate.setYLat("54.403159");

        SearchQuery searchQuery = SearchQuery.builder()
                .routesCount(3)
                .start(new Point("", startCoordinate))//FIXME: set region symbol
                .destination(new Point("", destinationCoordinate))//FIXME: set region symbol
                .timeOptions(new com.telegram.weather.bot.model.routes.TimeOptions(Instant.now(), com.telegram.weather.bot.model.routes.enums.QueryTimeType.DEPARTURE))//FIXME: time and type should be changeable
                .allowedRouteTypes(new com.telegram.weather.bot.model.routes.enums.RouteType[]{com.telegram.weather.bot.model.routes.enums.RouteType.PUBLIC_TRANSPORT})
                .realtimeSearchMode(SearchMode.NO_REALTIME)
                .publicTransportOptions(new com.telegram.weather.bot.model.routes.TransportOptions(com.telegram.weather.bot.model.routes.enums.AvoidChanges.DEFAULT, com.telegram.weather.bot.model.routes.enums.AccessibilityOptions.NONE))
                .userConnectionTypePreference(ConnectionTypePreference.OPTIMAL).build();

        JakdojadeQuery jakdojadeQuery = JakdojadeQuery.builder()
                .fetchType(FetchType.SYNC)
                .userLocation(null)
                .routesCorrelation(RoutesCorrelation.NONE)
                .searchQuery(searchQuery)
                .build();
        String json = ParseUtils.jakdojadeQueryToJson(jakdojadeQuery);
        System.out.println(json);

        assertTrue(json.length() > 0);
    }

    @Test
    public void parseAddresses() throws IOException {
        Addresses addresses = ParseUtils.parseAddresses(addressRes.getContent());
        System.out.println(addresses);

        assertNotNull(addresses);

        assertFalse(addresses.getLocations().isEmpty());

        assertNotNull(addresses.getLocations().get(0).getCoordinate());

        assertNotNull(addresses.getLocations().get(0).getAddress());


    }

    @Test
    public void testUrlAddress() {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(JakdojadeUtils.URI)
                .path(JakdojadeUtils.ADDRESSES)
                .queryParam("location", "54.4063304:18.5957872");
        System.out.println(builder.toUriString());
    }
}
