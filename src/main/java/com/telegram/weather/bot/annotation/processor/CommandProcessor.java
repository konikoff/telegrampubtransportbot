package com.telegram.weather.bot.annotation.processor;

import org.telegram.telegrambots.meta.api.objects.Update;

public interface CommandProcessor {

    void discoverCommands();
    Object executeCommand(final String state, final Update update, Object... args);
}
