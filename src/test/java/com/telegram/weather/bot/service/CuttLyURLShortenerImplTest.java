package com.telegram.weather.bot.service;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class CuttLyURLShortenerImplTest {

    @Test
    public void getShortenedURL() throws IOException {
        URLShortener shortener = new CuttLyURLShortenerImpl();

        String shortenedURL = shortener.getShortenedURL("https://www.youtube.com/channel/UCHK4HD0ltu1-I212icLPt3g");

        assertNotNull(shortenedURL);
    }
}
