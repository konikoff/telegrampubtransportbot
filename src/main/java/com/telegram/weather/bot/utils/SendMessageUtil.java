package com.telegram.weather.bot.utils;

import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.service.Emoji;
import com.telegram.weather.bot.service.LocalisationService;
import lombok.NonNull;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

public class SendMessageUtil {

    public static InlineKeyboardMarkup getMainMenuKeyboard(LocalisationService localisationService, String language) {
        InlineKeyboardMarkup replyKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineKeyboardButtons = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();

        String tripMessage = localisationService.getString("trip", language);
        row1.add(new InlineKeyboardButton()
                .setText(SendMessageUtil.getTripCommand(tripMessage))
                .setCallbackData(Command.NEW_TRIP.name()));

        String settingsCommand = localisationService.getString("settings", language);
        row1.add(new InlineKeyboardButton()
                .setText(SendMessageUtil.getSettingsCommand(settingsCommand))
                .setCallbackData(Command.SETTINGS.name()));

        inlineKeyboardButtons.add(row1);
        replyKeyboardMarkup.setKeyboard(inlineKeyboardButtons);
        return replyKeyboardMarkup;
    }

    public static SendMessage replyOnPreviousMsgMarkdown(@NonNull Message message) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        return sendMessage;
    }

    public static SendMessage replyOnPreviousMsgHtml(@NonNull Message message) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableHtml(true);
        sendMessage.setChatId(message.getChatId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        return sendMessage;
    }

    public static SendMessage sendHideKeyboard(Message message, @NonNull String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.enableMarkdown(true);
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);

        ReplyKeyboardRemove replyKeyboardRemove = new ReplyKeyboardRemove();
        replyKeyboardRemove.setSelective(true);
        sendMessage.setReplyMarkup(replyKeyboardRemove);

        return sendMessage;
    }

    public static String prepareHelpMessage(String baseString) {
        return String.format(baseString,
                Emoji.HEAVY_PLUS_SIGN.toString(),
                Emoji.EARTH_GLOBE_EUROPE_AFRICA.toString());
    }

    public static String getTripCommand(String baseString) {
        return String.format(baseString, Emoji.HEAVY_PLUS_SIGN.toString());
    }

    public static String getArrivalCommand(String baseString) {
        return String.format(baseString, Emoji.AIRPLANE_ARRIVAL.toString());
    }

    public static String getDepartureCommand(String baseString) {
        return String.format(baseString, Emoji.AIRPLANE_DEPARTURE.toString());
    }

    public static String getSettingsCommand(String baseString) {
        return String.format(baseString, Emoji.WRENCH.toString());
    }

    public static String getWarningMessage(String baseString) {
        return String.format(baseString,
                Emoji.WARNING.toString());
    }

    public static String getLanguagesCommand(String baseString) {
        return String.format(baseString,
                Emoji.GLOBE_WITH_MERIDIANS.toString());
    }

    public static String getChooseCity(String baseString) {
        return String.format(baseString,
                Emoji.CITY_SUNRISE.toString());
    }

    public static String getBackCommand(String baseString) {
        return String.format(baseString,
                Emoji.BACK_WITH_LEFTWARDS_ARROW_ABOVE.toString());
    }

    public static String getLocationCommand(String baseString) {
        return String.format(baseString,
                Emoji.ROUND_PUSHPIN.toString());
    }

    public static String getCancelCommand(String baseString) {
        return String.format(baseString,
                Emoji.CROSS_MARK.toString());
    }

    public static String getNowCommand(String baseString) {
        return String.format(baseString, Emoji.AIRPLANE_DEPARTURE.toString());
    }

    public static String getTripTimeTypeMessage(String baseString) {
        return String.format(baseString, Emoji.AIRPLANE_ARRIVAL.toString(), Emoji.AIRPLANE_DEPARTURE.toString(), Emoji.AIRPLANE_DEPARTURE.toString());
    }
}
