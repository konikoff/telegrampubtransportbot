package com.telegram.weather.bot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "User")
public class User extends BaseEntity {

    @Column(name = "userId")
    private Long userId;

    @Column(name = "language")
    private String language;

    @OneToOne
    private Chat chat;

    @OneToOne(cascade = CascadeType.ALL)
    private RegionCodes regionCodes;


    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Trip> trips = new HashSet<>();
}
