package com.telegram.weather.bot.commands;

import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.Trip;
import lombok.*;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RouteCommand {
    private Trip lastTrip;
    private Chat chat;
    private Instant dateTime;
}
