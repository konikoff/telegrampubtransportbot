package com.telegram.weather.bot.model.routes.enums;

public enum QueryTimeType {
    DEPARTURE, ARRIVAL
}
