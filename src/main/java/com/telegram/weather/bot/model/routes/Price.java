package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.DiscountId;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Price {
    private DiscountId discountId;
    private Integer priceCents;
    private String priceCurrency;
    private String discountName;
}
