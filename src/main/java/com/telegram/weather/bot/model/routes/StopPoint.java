package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.jakdojadequery.Coordinate;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StopPoint {
    private String stopName;
    private String stopPointCode;
    private Coordinate coordinate;
    private String codeInGroup;
    private String streetName;

}
