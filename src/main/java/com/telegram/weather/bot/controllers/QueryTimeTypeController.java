package com.telegram.weather.bot.controllers;

import com.telegram.weather.bot.annotation.BotCommand;
import com.telegram.weather.bot.annotation.BotController;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.commands.RouteCommand;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.model.Trip;
import com.telegram.weather.bot.model.routes.Route;
import com.telegram.weather.bot.model.routes.RoutePart;
import com.telegram.weather.bot.model.routes.RouteStop;
import com.telegram.weather.bot.model.routes.Routes;
import com.telegram.weather.bot.model.routes.enums.QueryTimeType;
import com.telegram.weather.bot.model.routes.enums.VehicleType;
import com.telegram.weather.bot.service.*;
import com.telegram.weather.bot.utils.SendMessageUtil;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.Locale;

@Slf4j
@BotController(value = StateCommand.TRIP_TIME_TYPE)
public class QueryTimeTypeController extends SendMessageExecutor {

    private final ChatService chatService;
    private final TemplateEngine botTemplateEngine;
    private final DefaultController defaultController;
    private final JakdojadeService jakdojadeService;
    private final TripService tripService;
    private final LocalisationService localisationService;
    private final MapService mapService;

    public QueryTimeTypeController(ChatService chatService,
                                   TemplateEngine botTemplateEngine,
                                   DefaultController defaultController,
                                   JakdojadeService jakdojadeService,
                                   TripService tripService,
                                   LocalisationService localisationService,
                                   MapService mapService) {
        this.chatService = chatService;
        this.botTemplateEngine = botTemplateEngine;
        this.defaultController = defaultController;
        this.jakdojadeService = jakdojadeService;
        this.tripService = tripService;
        this.localisationService = localisationService;
        this.mapService = mapService;
    }

    @BotCommand(command = Command.DEFAULT)
    public void defaultCommand(Message message, String language) throws TelegramApiException {
        if (message.hasText()) {
            String dateTimeText = message.getText();

            Chat savedChat = this.chatService.getChat(message);
            Trip lastTrip = this.tripService.getLastTrip(savedChat);

            Instant dateTime = null;
            try {
                LocalTime localDate = LocalTime.from(DateTimeFormatter.ISO_TIME.parse(dateTimeText));
                dateTime = localDate.atDate(LocalDate.now()).atZone(ZoneId.of("Europe/Warsaw")).toInstant();

            } catch (DateTimeParseException e1) {

                try {
                    DateTimeFormatter parseFormatter = new DateTimeFormatterBuilder()
                            .appendPattern("MM-dd HH:mm")
                            .parseDefaulting(ChronoField.YEAR, LocalDate.now().getYear())
                            .toFormatter(Locale.ENGLISH);

                    LocalDateTime localDateTime = LocalDateTime.from(parseFormatter.parse(dateTimeText));

                    dateTime = localDateTime.atZone(ZoneId.of("Europe/Warsaw")).toInstant();

                } catch (DateTimeParseException e2) {
                    log.error("Unable to parse DATE and TIME from: " + dateTimeText + "\n" + e2.getMessage());

                    getSender().execute(getRequestTimeMessage(message, language));
                }
            }

            if (dateTime != null) {
                this.chatService.updateStatus(message, StateCommand.MAIN_MENU);

                RouteCommand routeCommand = RouteCommand.builder()
                        .chat(savedChat)
                        .lastTrip(lastTrip)
                        .dateTime(dateTime)
                        .build();

                sendTripEndMessage(message, routeCommand, language);
            }

        }

        getSender().execute(getRequestTimeMessage(message, language));
    }

    @BotCommand(name = "arrival", emoji = Emoji.AIRPLANE_ARRIVAL, command = Command.ARRIVAL)
    public void arrivalHandler(Message message, String language) throws TelegramApiException {
        Chat savedChat = this.chatService.getChat(message);
        Trip lastTrip = this.tripService.getLastTrip(savedChat);
        lastTrip.setQueryTimeType(QueryTimeType.ARRIVAL);
        this.tripService.save(lastTrip);

        getSender().execute(getRequestTimeMessage(message, language));
    }

    @BotCommand(name = "departure", emoji = Emoji.AIRPLANE_DEPARTURE, command = Command.DEPARTURE)
    public void departureHandler(Message message, String language) throws TelegramApiException {
        Chat savedChat = this.chatService.getChat(message);
        Trip lastTrip = this.tripService.getLastTrip(savedChat);
        lastTrip.setQueryTimeType(QueryTimeType.DEPARTURE);
        this.tripService.save(lastTrip);

        getSender().execute(getRequestTimeMessage(message, language));
    }

    @BotCommand(name = "now", emoji = Emoji.AIRPLANE_DEPARTURE, command = Command.NOW)
    public void nowHandler(Message message, String language) throws TelegramApiException {
        this.chatService.updateStatus(message, StateCommand.MAIN_MENU);

        Chat savedChat = this.chatService.getChat(message);
        Trip lastTrip = this.tripService.getLastTrip(savedChat);
        lastTrip.setQueryTimeType(QueryTimeType.DEPARTURE);

        RouteCommand routeCommand = RouteCommand.builder()
                .chat(savedChat)
                .lastTrip(lastTrip)
                .dateTime(Instant.now())
                .build();

        sendRouteMessage(message, routeCommand, language);
    }

    @BotCommand(name = "cancel", emoji = Emoji.CROSS_MARK, command = Command.CANCEL)
    public void cancelHandler(Message message, String language) throws TelegramApiException {
        this.chatService.updateStatus(message, StateCommand.MAIN_MENU);

        SendMessage sendMessage = SendMessageUtil.replyOnPreviousMsgMarkdown(message);
        sendMessage.setReplyMarkup(SendMessageUtil.getMainMenuKeyboard(localisationService, language));

        getSender().execute(sendMessage);
    }


    private SendPhoto sendTripEndMessage(Message message, RouteCommand routeCommand, String language) {
        Routes routes = this.jakdojadeService.getRoutes(routeCommand);
        final Context ctx = new Context(); //TODO: set language


        for (Route route : routes.getRoutes()) {
            ctx.setVariable("route", route);
            ctx.setVariable("trip", routeCommand.getLastTrip());
            String outMessageText = botTemplateEngine.process("route/routes_message", ctx);

            Trip lastTrip = routeCommand.getLastTrip();

            SendPhoto sendPhoto = new SendPhoto();
            sendPhoto
                    .setChatId(message.getChatId())
                    .setReplyToMessageId(message.getMessageId())
                    .setCaption(outMessageText)
                    .setReplyMarkup(SendMessageUtil.getMainMenuKeyboard(localisationService, language))
                    .setPhoto(route.getRouteId(), mapService.getImageByRoute(lastTrip, route));

            try {
                getSender().execute(sendPhoto);
            } catch (TelegramApiException e) {
                log.error(e.getMessage());
            }
        }

        throw new RuntimeException("No routes");
    }

    private SendMessage getRequestTimeMessage(Message message, String language) {
        String messageText = localisationService.getString("requestTime", language);
        return SendMessageUtil.sendHideKeyboard(message, messageText);
    }


    private void sendRouteMessage(Message message, RouteCommand routeCommand, String language) {
        Routes routes = this.jakdojadeService.getRoutes(routeCommand);

        for (Route route : routes.getRoutes()) {
            String outMessageText = assembleRoutesMessage(routeCommand.getLastTrip(), route, language);

            Trip lastTrip = routeCommand.getLastTrip();

            SendPhoto sendPhoto = new SendPhoto();
            sendPhoto
                    .setChatId(message.getChatId())
                    .setReplyToMessageId(message.getMessageId())
                    //.setCaption(outMessageText)
                    .setReplyMarkup(SendMessageUtil.getMainMenuKeyboard(localisationService, language))
                    .setPhoto(route.getRouteId(), mapService.getImageByRoute(lastTrip, route));

            try {
                getSender().execute(sendPhoto);
                getSender().execute(new SendMessage()
                        .setChatId(message.getChatId())
                        .setText(outMessageText)
                );
            } catch (TelegramApiException e) {
                log.error(e.getMessage());
            }
        }
    }

    private String assembleRoutesMessage(Trip trip, Route route, String language) {
        StringBuilder assembledMessage = new StringBuilder();
        assembledMessage
                .append(getAddressMessage(trip, language))
                .append("\n")
                .append(getRouteMessage(route, language));

        return assembledMessage.toString();
    }

    public String getAddressMessage(Trip trip, String language) {
        String baseString = localisationService.getString("routeAddressMessage", language);
        return String.format(baseString,
                trip.getStartPoint().getRoad() == null ? trip.getStartPoint().getGeoPoint().getLatitude() : trip.getStartPoint().getRoad(),
                trip.getStartPoint().getHouseNumber() == null ? trip.getStartPoint().getGeoPoint().getLongitude() : trip.getStartPoint().getHouseNumber(),
                trip.getStartPoint().getCity() == null ? "" : trip.getStartPoint().getCity(),

                trip.getEndPoint().getRoad() == null ? trip.getEndPoint().getGeoPoint().getLatitude() : trip.getEndPoint().getRoad(),
                trip.getEndPoint().getHouseNumber() == null ? trip.getEndPoint().getGeoPoint().getLongitude() : trip.getEndPoint().getHouseNumber(),
                trip.getEndPoint().getCity() == null ? "" : trip.getEndPoint().getCity());
    }

    public String getRouteMessage(Route route, String language) {
        StringBuilder assembledRouteMessage = new StringBuilder();
        for (RoutePart part : route.getRouteParts()) {

            if (part.getRouteVehicle() != null && !part.getRouteVehicle().getRouteStops().isEmpty()) {
                VehicleType vehicleType = part.getRouteVehicle().getRouteLine().getLine().getVehicleType();
                assembledRouteMessage
                        .append(vehicleType == VehicleType.VEHICLE_TYPE_BUS ? "\nBus " : "\nPublic transport ")
                        .append(part.getRouteVehicle().getRouteLine().getLine().getName())
                        .append("\t")
                        .append("Direction: ")
                        .append(part.getRouteVehicle().getRouteLine().getLineHeadingText());

                Integer stopsStartIndex = part.getRouteVehicle().getStopsStartIndex();
                Integer stopsEndIndex = part.getRouteVehicle().getStopsEndIndex();

                int counter = 1;
                for (int i = 0; i < part.getRouteVehicle().getRouteStops().size(); i++) {
                    if (i >= stopsStartIndex && i <= stopsEndIndex) {
                        RouteStop routeStop = part.getRouteVehicle().getRouteStops().get(i);
                        assembledRouteMessage
                                .append("\n\t\t")
                                .append(counter++)
                                .append(". ")
                                .append(routeStop.getLineStop().getStopPoint().getStopName());
                    }

                    /**
                     * Optional. Caption for the animation, audio, document, photo, video or voice, 0-1024 characters
                     */
                }
            }

        }
        assembledRouteMessage
                .append("\n\n")
                .append(route.getRouteDescription());
        return assembledRouteMessage.toString();
    }
}
