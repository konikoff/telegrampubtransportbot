package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.Category;
import com.telegram.weather.bot.model.routes.enums.Purchasable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@ToString
public class TicketType {
    private String ticketTypeId;
    private String ticketTypeName;
    private Category category;
    private String categoryName;
    private Integer orderNumber;
    private String ticketsAuthoritySymbol;
    private String ticketsAuthorityName;
    private Instant typeValidFromDate;
    private Purchasable purchasable;
    private Boolean requiresJourney;
    private List<Constraint> constraints;
    private List<Parameter> parameters;
    private List<Price> prices;
    private DisplayModel displayModel;
    private Integer maxQuantityInOrder;

}
