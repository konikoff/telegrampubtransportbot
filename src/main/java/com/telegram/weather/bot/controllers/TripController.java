package com.telegram.weather.bot.controllers;

import com.telegram.weather.bot.annotation.BotCommand;
import com.telegram.weather.bot.annotation.BotController;
import com.telegram.weather.bot.commands.Command;
import com.telegram.weather.bot.model.Chat;
import com.telegram.weather.bot.model.GeoPoint;
import com.telegram.weather.bot.model.StateCommand;
import com.telegram.weather.bot.service.ChatService;
import com.telegram.weather.bot.service.Emoji;
import com.telegram.weather.bot.service.LocalisationService;
import com.telegram.weather.bot.service.TripService;
import com.telegram.weather.bot.utils.SendMessageUtil;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@BotController(value = StateCommand.TRIP)
public class TripController extends SendMessageExecutor {

    private final MainMenuController mainMenuController;
    private final ChatService chatService;
    private final LocalisationService localisationService;
    private final TripService tripService;
    private final DefaultController defaultController;

    public TripController(MainMenuController mainMenuController,
                          ChatService chatService,
                          LocalisationService localisationService,
                          TripService tripService,
                          DefaultController defaultController) {
        this.mainMenuController = mainMenuController;
        this.chatService = chatService;
        this.localisationService = localisationService;
        this.tripService = tripService;
        this.defaultController = defaultController;
    }

    @BotCommand(command = Command.DEFAULT)
    public void defaultCommand(Message message, String language) throws TelegramApiException {
        System.out.println("Expecting locations");

        if (message.hasLocation()) {
            Chat savedChat = chatService.getChat(message);
            chatService.updateStatus(message, StateCommand.TRIP_LOCATION_END);

            final Float longitude = message.getLocation().getLongitude();
            final Float latitude = message.getLocation().getLatitude();
            tripService.saveStartPoint(new GeoPoint(longitude, latitude), savedChat);

            getSender().execute(SendMessageUtil.sendHideKeyboard(message, localisationService.getString("endTripMessage", language)));
        } else {
            this.mainMenuController.newTrip(message, language);
        }
    }

    @BotCommand(name = "back", emoji = Emoji.BACK_WITH_LEFTWARDS_ARROW_ABOVE, command = Command.BACK)
    public void back(Message message, String language) throws TelegramApiException {
        defaultController.sendMessageDefault(message, language);
    }
}
