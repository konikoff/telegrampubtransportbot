package com.telegram.weather.bot.model.routes;

import com.telegram.weather.bot.model.routes.enums.RealtimeStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RealtimeInformation {
    private RealtimeStatus realtimeStatus;
    private Boolean routeUnrealizable;
}
